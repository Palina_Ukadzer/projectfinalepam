package com.epam.ukader.DAO;

import com.epam.ukader.myExeptions.daoException.ClosingConnectionException;
import com.epam.ukader.myExeptions.daoException.DBAccessException;

import java.util.Collection;

/**
 * Created by Palina Ukadzer on 31.05.2015.
 * DAO interface
 */
public interface ObjectDAO<T> {

    T getById(int entityId) throws DBAccessException, ClosingConnectionException;

    void save(T entity) throws DBAccessException, ClosingConnectionException;

    void update(T entity) throws DBAccessException, ClosingConnectionException;

    void delete(int entityId) throws DBAccessException, ClosingConnectionException;

    Collection<T> getAll(Collection<T> all) throws DBAccessException, ClosingConnectionException;

}
