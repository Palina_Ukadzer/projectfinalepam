package com.epam.ukader.DAO.implementation;

import com.epam.ukader.DAO.entity.ApartmentClassDAO;
import com.epam.ukader.bean.Apartment;
import com.epam.ukader.bean.ApartmentClass;
import com.epam.ukader.bean.builder.ApartmentClassBuilder;
import com.epam.ukader.myExeptions.daoException.ClosingConnectionException;
import com.epam.ukader.myExeptions.daoException.DBAccessException;
import com.epam.ukader.service.MyConnectionPool;
import com.epam.ukader.service.ResourceManager;

import java.sql.*;
import java.util.Collection;

/**
 * Created by Palina Ukadzer on 19.06.2015.
 * Implements ApartmentClassDAO for mysql db
 */
public class MysqlApartmentClassDAO implements ApartmentClassDAO {

    private Connection connection;
    private ResourceManager resourceManager = ResourceManager.INSTANCE;


    @Override
    public ApartmentClass getById(int entityId) throws DBAccessException, ClosingConnectionException {
        ApartmentClassBuilder apartmentClassBuilder = new ApartmentClassBuilder();
        try {
            connection = MyConnectionPool.getConnectionFromPool();
            PreparedStatement preparedStatement = connection.prepareStatement(resourceManager.getString("SELECT_FROM_CLASS_ID"));
            preparedStatement.setInt(1, entityId);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                return apartmentClassBuilder.setId(rs.getInt(1)).setName_english(rs.getString(2))
                        .setName_russian(rs.getString(3)).createApartmentClass();
            } else {
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBAccessException();
        } finally {
            MyConnectionPool.returnConnectionToPool(connection);
        }
    }

    @Override
    public void save(ApartmentClass entity) throws DBAccessException, ClosingConnectionException {
        try {
            connection = MyConnectionPool.getConnectionFromPool();
            PreparedStatement preparedStatement = connection.prepareStatement(resourceManager.getString("INSERT_INTO_CLASS"));

            preparedStatement.setString(1, entity.getName_english());
            preparedStatement.setString(2, entity.getName_russian());
            preparedStatement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBAccessException();
        } finally {
            MyConnectionPool.returnConnectionToPool(connection);
        }
    }

    @Override
    public void update(ApartmentClass entity) throws DBAccessException, ClosingConnectionException {
        try {
            connection = MyConnectionPool.getConnectionFromPool();
            PreparedStatement preparedStatement = connection.prepareStatement(resourceManager.getString("UPDATE_CLASS"));
            preparedStatement.setString(1, entity.getName_english());
            preparedStatement.setString(2, entity.getName_russian());
            preparedStatement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBAccessException();
        } finally {
            MyConnectionPool.returnConnectionToPool(connection);
        }
    }

    @Override
    public void delete(int entityId) throws DBAccessException, ClosingConnectionException {
        try {
            connection = MyConnectionPool.getConnectionFromPool();
            PreparedStatement preparedStatement = connection.prepareStatement(resourceManager.getString("DELETE_CLASS"));
            preparedStatement.setInt(1, entityId);
            preparedStatement.executeQuery();
            return;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBAccessException();
        } finally {
            MyConnectionPool.returnConnectionToPool(connection);
        }
    }

    @Override
    public Collection<ApartmentClass> getAll(Collection<ApartmentClass> all) throws DBAccessException, ClosingConnectionException {
        ApartmentClassBuilder apartmentClassBuilder = new ApartmentClassBuilder();
        all.clear();
        try {
            connection = MyConnectionPool.getConnectionFromPool();
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(resourceManager.getString("SELECT_FROM_CLASS"));
            while (rs.next()) {
                apartmentClassBuilder.setId(rs.getInt(1)).setName_english(rs.getString(2)).setName_russian(rs.getString(3));
                all.add(apartmentClassBuilder.createApartmentClass());
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBAccessException();
        } finally {
            MyConnectionPool.returnConnectionToPool(connection);
        }
        return all;
    }
}
