package com.epam.ukader.DAO.implementation;

import com.epam.ukader.DAO.entity.OrderDAO;
import com.epam.ukader.bean.Order;
import com.epam.ukader.bean.builder.OrderBuilder;
import com.epam.ukader.myExeptions.daoException.ClosingConnectionException;
import com.epam.ukader.myExeptions.daoException.DBAccessException;
import com.epam.ukader.service.MyConnectionPool;
import com.epam.ukader.service.ResourceManager;

import java.sql.*;
import java.util.Calendar;
import java.util.Collection;

/**
 * Created by Palina Ukadzer on 02.06.2015.
 * Implements OrderDAO for mysql db
 */
public class MysqlOrderDAO implements OrderDAO {

    private static Connection connection = null;
    private ResourceManager resourceManager = ResourceManager.INSTANCE;

    @Override
    public Order getById(int entityId) throws DBAccessException, ClosingConnectionException {
        OrderBuilder orderBuilder = new OrderBuilder();
        try {
            connection = MyConnectionPool.getConnectionFromPool();
            PreparedStatement preparedStatement = connection.prepareStatement(resourceManager
                    .getString("SELECT_FROM_ORDER_ID"));
            preparedStatement.setInt(1, entityId);
            ResultSet rs = preparedStatement.executeQuery();
            if(rs.next()) {
                return orderBuilder.setId(rs.getInt(1)).setPeopleNumber(rs.getInt(2))
                        .setHumanId(rs.getInt(3)).setClassId(rs.getInt(4)).setTimeIn(rs.getDate(5))
                        .setTimeOut(rs.getDate(6)).setStatusId(rs.getInt(7)).createOrder();
            }else {
                return null;
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBAccessException();
        } finally {
            MyConnectionPool.returnConnectionToPool(connection);
        }
    }

    @Override
    public void save(Order entity) throws DBAccessException, ClosingConnectionException {
        try {
            connection = MyConnectionPool.getConnectionFromPool();
            PreparedStatement preparedStatement = connection.prepareStatement(resourceManager.getString("INSERT_INTO_ORDER"));
            preparedStatement.setInt(1, entity.getPeopleNumber());
            preparedStatement.setInt(2, entity.getHumanId());
            preparedStatement.setInt(3, entity.getClassId());
            preparedStatement.setDate(4, utilToSql(entity.getTimeIn()));
            preparedStatement.setDate(5, utilToSql(entity.getTimeOut()));
            preparedStatement.setInt(6, entity.getStatusId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBAccessException();
        } finally {
            MyConnectionPool.returnConnectionToPool(connection);
        }

    }

    @Override
    public void update(Order entity) throws DBAccessException, ClosingConnectionException {
        try {
            connection = MyConnectionPool.getConnectionFromPool();
            PreparedStatement preparedStatement = connection.prepareStatement(resourceManager.getString("UPDATE_ORDER"));
            preparedStatement.setInt(1, entity.getPeopleNumber());
            preparedStatement.setInt(2, entity.getHumanId());
            preparedStatement.setInt(3, entity.getClassId());
            preparedStatement.setDate(4, utilToSql(entity.getTimeIn()));
            preparedStatement.setDate(5, utilToSql(entity.getTimeOut()));
            preparedStatement.setInt(6, entity.getStatusId());
            preparedStatement.setInt(7, entity.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBAccessException();
        } finally {
            MyConnectionPool.returnConnectionToPool(connection);
        }
    }

    @Override
    public void delete(int entityId) throws DBAccessException, ClosingConnectionException {
        try {
            connection = MyConnectionPool.getConnectionFromPool();
            PreparedStatement preparedStatement = connection.prepareStatement(resourceManager.getString("DELETE_ORDER"));
            preparedStatement.setInt(1, entityId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBAccessException();
        } finally {
            MyConnectionPool.returnConnectionToPool(connection);
        }
    }

    public Collection<Order> getAllUndoneForId(Collection<Order> all, int id) throws DBAccessException, ClosingConnectionException {
        OrderBuilder orderBuilder = new OrderBuilder();
        all.clear();
        try {
            connection = MyConnectionPool.getConnectionFromPool();
            PreparedStatement statement = connection.prepareStatement(resourceManager.getString("SELECT_FROM_ORDER_WITH_STATUS_FOR_HUMAN"));
            statement.setInt(1, id);
            statement.setInt(2, 1);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                orderBuilder.setId(rs.getInt(1)).setPeopleNumber(rs.getInt(2))
                        .setHumanId(rs.getInt(3)).setClassId(rs.getInt(4)).setTimeIn(rs.getDate(5))
                        .setTimeOut(rs.getDate(6)).setStatusId(rs.getInt(7));
                all.add(orderBuilder.createOrder());
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBAccessException();
        } finally {
            MyConnectionPool.returnConnectionToPool(connection);
        }
        return all;
    }


    @Override
    public Collection<Order> getAll(Collection<Order> all) throws DBAccessException, ClosingConnectionException {
        OrderBuilder orderBuilder = new OrderBuilder();
        all.clear();
        try {
            connection = MyConnectionPool.getConnectionFromPool();
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(resourceManager.getString("SELECT_FROM_ORDER"));
            while (rs.next()) {
                orderBuilder.setId(rs.getInt(1)).setPeopleNumber(rs.getInt(2))
                        .setHumanId(rs.getInt(3)).setClassId(rs.getInt(4)).setTimeIn(rs.getDate(5))
                        .setTimeOut(rs.getDate(6)).setStatusId(rs.getInt(7));
                all.add(orderBuilder.createOrder());
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBAccessException();
        } finally {
            MyConnectionPool.returnConnectionToPool(connection);
        }
        return all;
    }

    public Collection<Order> getNotAcceptedOrders(Collection<Order> all) throws DBAccessException, ClosingConnectionException {
        OrderBuilder orderBuilder = new OrderBuilder();
        all.clear();
        try {
            connection = MyConnectionPool.getConnectionFromPool();
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(resourceManager.getString("SELECT_FROM_ORDER_WITH_STATUS_1"));
            while (rs.next()) {
                orderBuilder.setId(rs.getInt(1)).setPeopleNumber(rs.getInt(2))
                        .setHumanId(rs.getInt(3)).setClassId(rs.getInt(4)).setTimeIn(rs.getDate(5))
                        .setTimeOut(rs.getDate(6)).setStatusId(rs.getInt(7));
                all.add(orderBuilder.createOrder());
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBAccessException();
        } finally {
            MyConnectionPool.returnConnectionToPool(connection);
        }
        return all;
    }

    private Date utilToSql(java.util.Date date) {
        Calendar cal = Calendar.getInstance();
        java.util.Date utilDate = date;
        cal.setTime(utilDate);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        Date sqlDate = new Date(cal.getTime().getTime());
        return sqlDate;
    }

}

