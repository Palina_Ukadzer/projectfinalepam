package com.epam.ukader.DAO.implementation;

import com.epam.ukader.DAO.entity.apartmentDAO;
import com.epam.ukader.bean.Apartment;
import com.epam.ukader.bean.builder.ApartmentBuilder;
import com.epam.ukader.myExeptions.daoException.ClosingConnectionException;
import com.epam.ukader.myExeptions.daoException.DBAccessException;
import com.epam.ukader.service.MyConnectionPool;
import com.epam.ukader.service.ResourceManager;

import java.sql.*;
import java.util.Collection;

/**
 * Created by Palina Ukadzer on 02.06.2015.
 * Implements ApartmentDAO for mysql db
 */
public class MysqlApartmentDAO implements apartmentDAO {

    private static Connection connection;
    private ResourceManager resourceManager = ResourceManager.INSTANCE;

    @Override
    public Apartment getById(int entityId) throws DBAccessException, ClosingConnectionException {
        ApartmentBuilder apartmentBuilder = new ApartmentBuilder();
        Statement statement;
        try {
            connection = MyConnectionPool.getConnectionFromPool();
            PreparedStatement preparedStatement = connection.prepareStatement(resourceManager.getString("SELECT_FROM_APARTMENT_ID"));
            preparedStatement.setInt(1, entityId);
            ResultSet rs = preparedStatement.executeQuery();
            Apartment apartment = new Apartment();
            if(rs.next()) {
                apartment = apartmentBuilder.setId(rs.getInt(1)).setPeopleNumber(rs.getInt(2))
                        .setPrice(rs.getInt(3)).setClassId(rs.getInt(4)).createApartment();
                return apartment;
            }else {
                return null;
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBAccessException();
        } finally {
            MyConnectionPool.returnConnectionToPool(connection);
        }
    }

    @Override
    public void save(Apartment entity) throws DBAccessException, ClosingConnectionException {
        try {
            connection = MyConnectionPool.getConnectionFromPool();
            PreparedStatement preparedStatement = connection.prepareStatement(resourceManager.getString("INSERT_INTO_APARTMENT"));
            preparedStatement.setInt(1, entity.getPeopleNumber());
            preparedStatement.setInt(2, entity.getPrice());
            preparedStatement.setInt(3, entity.getClassId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBAccessException();
        } finally {
            MyConnectionPool.returnConnectionToPool(connection);
        }
    }

    @Override
    public void update(Apartment entity) throws DBAccessException, ClosingConnectionException {
        try {
            connection = MyConnectionPool.getConnectionFromPool();
            PreparedStatement preparedStatement = connection.prepareStatement(resourceManager.getString("UPDATE_APARTMENT"));
            preparedStatement.setInt(1, entity.getPeopleNumber());
            preparedStatement.setInt(2, entity.getPrice());
            preparedStatement.setInt(3, entity.getClassId());
            preparedStatement.setInt(4, entity.getId());
            preparedStatement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBAccessException();
        } finally {
            MyConnectionPool.returnConnectionToPool(connection);
        }
    }

    @Override
    public void delete(int entityId) throws DBAccessException, ClosingConnectionException {
        try {
            connection = MyConnectionPool.getConnectionFromPool();
            PreparedStatement preparedStatement = connection.prepareStatement(resourceManager.getString("DELETE_APARTMENT"));
            preparedStatement.setInt(1, entityId);
            preparedStatement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBAccessException();
        } finally {
            MyConnectionPool.returnConnectionToPool(connection);
        }
    }

    @Override
    public Collection<Apartment> getAll(Collection<Apartment> all) throws DBAccessException, ClosingConnectionException {
        ApartmentBuilder apartmentBuilder = new ApartmentBuilder();
        all.clear();
        try {
            connection = MyConnectionPool.getConnectionFromPool();
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(resourceManager.getString("SELECT_FROM_APARTMENT"));
            while (rs.next()) {
                apartmentBuilder.setId(rs.getInt(1)).setPeopleNumber(rs.getInt(2))
                        .setPrice(rs.getInt(3)).setClassId(rs.getInt(4)).createApartment();
                all.add(apartmentBuilder.createApartment());
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBAccessException();
        } finally {
            MyConnectionPool.returnConnectionToPool(connection);
        }
        return all;
    }

}
