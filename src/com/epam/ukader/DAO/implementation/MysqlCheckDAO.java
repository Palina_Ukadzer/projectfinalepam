package com.epam.ukader.DAO.implementation;

import com.epam.ukader.DAO.entity.CheckDAO;
import com.epam.ukader.bean.Check;
import com.epam.ukader.bean.builder.CheckBuilder;
import com.epam.ukader.myExeptions.daoException.ClosingConnectionException;
import com.epam.ukader.myExeptions.daoException.DBAccessException;
import com.epam.ukader.service.MyConnectionPool;
import com.epam.ukader.service.ResourceManager;

import java.sql.*;
import java.util.Collection;

/**
 * Created by Palina Ukadzer on 02.06.2015.
 * Implements CheckDAO for mysql db
 */
public class MysqlCheckDAO implements CheckDAO {

    private static Connection connection = null;
    private ResourceManager resourceManager = ResourceManager.INSTANCE;

    @Override
    public Check getById(int entityId) throws DBAccessException, ClosingConnectionException {
        CheckBuilder checkBuilder = new CheckBuilder();
        try {
            connection = MyConnectionPool.getConnectionFromPool();
            PreparedStatement preparedStatement = connection.prepareStatement(resourceManager
                    .getString("SELECT_FROM_CHECK_ID"));
            preparedStatement.setInt(1, entityId);
            ResultSet rs = preparedStatement.executeQuery();
            if(rs.next()) {
                return checkBuilder.setId(rs.getInt(1)).setPrice(rs.getInt(2))
                        .setOrderId(rs.getInt(3)).setApartmentId(rs.getInt(4)).createCheck();
            }else {
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBAccessException();
        } finally {
            MyConnectionPool.returnConnectionToPool(connection);
        }
    }

    @Override
    public void save(Check entity) throws DBAccessException, ClosingConnectionException {
        try {
            connection = MyConnectionPool.getConnectionFromPool();
            PreparedStatement preparedStatement = connection.prepareStatement(resourceManager.getString("INSERT_INTO_CHECK"));
            preparedStatement.setInt(1, entity.getPrice());
            preparedStatement.setInt(2, entity.getOrderId());
            preparedStatement.setInt(3, entity.getApartmentId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBAccessException();
        } finally {
            MyConnectionPool.returnConnectionToPool(connection);
        }

    }


    @Override
    public void update(Check entity) throws DBAccessException, ClosingConnectionException {
        try {
            connection = MyConnectionPool.getConnectionFromPool();
            PreparedStatement preparedStatement = connection.prepareStatement(resourceManager.getString("UPDATE_CHECK"));
            preparedStatement.setInt(1, entity.getPrice());
            preparedStatement.setInt(2, entity.getOrderId());
            preparedStatement.setInt(3, entity.getApartmentId());
            preparedStatement.setInt(4, entity.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBAccessException();
        } finally {
            MyConnectionPool.returnConnectionToPool(connection);
        }
    }

    @Override
    public void delete(int entityId) throws DBAccessException, ClosingConnectionException {
        try {
            connection = MyConnectionPool.getConnectionFromPool();
            PreparedStatement preparedStatement = connection.prepareStatement(resourceManager.getString("DELETE_CHECK"));
            preparedStatement.setInt(1, entityId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBAccessException();
        } finally {
            MyConnectionPool.returnConnectionToPool(connection);
        }
    }

    @Override
    public Collection<Check> getAll(Collection<Check> all) throws DBAccessException, ClosingConnectionException {
        CheckBuilder checkBuilder = new CheckBuilder();
        all.clear();
        try {
            connection = MyConnectionPool.getConnectionFromPool();
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(resourceManager.getString("SELECT_FROM_CHECK"));
            while (rs.next()) {
                checkBuilder.setId(rs.getInt(1)).setPrice(rs.getInt(2))
                        .setOrderId(rs.getInt(3)).setApartmentId(rs.getInt(4));
                all.add(checkBuilder.createCheck());
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBAccessException();
        } finally {
            MyConnectionPool.returnConnectionToPool(connection);
        }
        return all;
    }

    public Collection<Check> getAllForApartment(Collection<Check> all, int id) throws DBAccessException, ClosingConnectionException {
        CheckBuilder checkBuilder = new CheckBuilder();
        all.clear();
        try {
            connection = MyConnectionPool.getConnectionFromPool();
            PreparedStatement statement = connection.prepareStatement(resourceManager.getString("SELECT_FROM_CHECK_FOR_APARTMENT"));
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                checkBuilder.setId(rs.getInt(1)).setPrice(rs.getInt(2))
                        .setOrderId(rs.getInt(3)).setApartmentId(rs.getInt(4));
                all.add(checkBuilder.createCheck());
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBAccessException();
        } finally {
            MyConnectionPool.returnConnectionToPool(connection);
        }
        return all;
    }

    public Collection<Check> getAllForUser(Collection<Check> all, int id) throws DBAccessException, ClosingConnectionException {
        CheckBuilder checkBuilder = new CheckBuilder();
        all.clear();
        try {
            connection = MyConnectionPool.getConnectionFromPool();
            Statement statement = connection.createStatement();
            PreparedStatement preparedStatement = connection.prepareStatement(resourceManager.getString("SELECT_FROM_ORDER_ID"));
            ResultSet rs = statement.executeQuery(resourceManager.getString("SELECT_FROM_CHECK"));
            while (rs.next()) {
                preparedStatement.setInt(1, rs.getInt(3));
                ResultSet resultSet = preparedStatement.executeQuery();
                resultSet.next();
                if (resultSet.getInt(3) == id) {
                    checkBuilder.setId(rs.getInt(1)).setPrice(rs.getInt(2))
                            .setOrderId(rs.getInt(3)).setApartmentId(rs.getInt(4));
                    all.add(checkBuilder.createCheck());
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBAccessException();
        } finally {
            MyConnectionPool.returnConnectionToPool(connection);
        }
        return all;
    }

}
