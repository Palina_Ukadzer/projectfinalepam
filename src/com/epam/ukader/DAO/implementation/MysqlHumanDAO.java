package com.epam.ukader.DAO.implementation;

import com.epam.ukader.DAO.entity.HumanDAO;
import com.epam.ukader.bean.Human;
import com.epam.ukader.bean.builder.HumanBuilder;
import com.epam.ukader.myExeptions.daoException.ClosingConnectionException;
import com.epam.ukader.myExeptions.daoException.DBAccessException;
import com.epam.ukader.service.MyConnectionPool;
import com.epam.ukader.service.ResourceManager;

import java.sql.*;
import java.util.Collection;

/**
 * Created by Palina Ukadzer on 01.06.2015.
 * Implements HumanDAO for mysql db
 */
public class MysqlHumanDAO implements HumanDAO {


    private static Connection connection = null;
    private ResourceManager resourceManager = ResourceManager.INSTANCE;


    public Human getByEmailPassword(String email, String password) throws DBAccessException, ClosingConnectionException {
        HumanBuilder humanBuilder = new HumanBuilder();
        try {
            connection = MyConnectionPool.getConnectionFromPool();
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(resourceManager.getString("SELECT_FROM_HUMAN"));
            while (rs.next()) {
                if (rs.getString(4).equals(email) && rs.getString(6).equals(password)) {
                    humanBuilder.setEmail(email).setId(rs.getInt(1)).setName(rs.getString(2)).setSurname(rs.getString(3))
                            .setPassword(password).setPhone(rs.getString(5)).setRoleId(rs.getInt(7));
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBAccessException();
        } finally {
            MyConnectionPool.returnConnectionToPool(connection);
        }
        return humanBuilder.createHuman();
    }


    @Override
    public Human getById(int entityId) throws DBAccessException, ClosingConnectionException {
        try {
            connection = MyConnectionPool.getConnectionFromPool();
            PreparedStatement preparedStatement = connection.prepareStatement(resourceManager.getString("SELECT_FROM_HUMAN_WHERE_ID"));
            preparedStatement.setInt(1, entityId);
            ResultSet rs = preparedStatement.executeQuery();
            if(rs.next()) {
                return new HumanBuilder().setEmail(rs.getString(4)).setId(rs.getInt(1)).setName(rs.getString(2))
                        .setSurname(rs.getString(3)).setPassword(rs.getString(6))
                        .setPhone(rs.getString(4)).setRoleId(rs.getInt(7)).createHuman();
            }else {
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBAccessException();
        } finally {
            MyConnectionPool.returnConnectionToPool(connection);
        }
    }


    @Override
    public void save(Human entity) throws DBAccessException, ClosingConnectionException {
        try {
            connection = MyConnectionPool.getConnectionFromPool();
            PreparedStatement preparedStatement = connection.prepareStatement(resourceManager.getString("INSERT_INTO_HUMAN"));
            preparedStatement.setString(1, entity.getName());
            preparedStatement.setString(2, entity.getSurname());
            preparedStatement.setString(3, entity.getEmail());
            preparedStatement.setString(4, entity.getPhone());
            preparedStatement.setString(5, entity.getPassword());
            preparedStatement.setInt(6, 2);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBAccessException();
        } finally {
            MyConnectionPool.returnConnectionToPool(connection);
        }
    }

    @Override
    public void update(Human entity) throws DBAccessException, ClosingConnectionException {
        try {
            connection = MyConnectionPool.getConnectionFromPool();
            PreparedStatement preparedStatement = connection.prepareStatement(resourceManager.getString("UPDATE_HUMAN"));
            preparedStatement.setString(1, entity.getName());
            preparedStatement.setString(2, entity.getSurname());
            preparedStatement.setString(3, entity.getEmail());
            preparedStatement.setString(4, entity.getPhone());
            preparedStatement.setString(5, entity.getPassword());
            preparedStatement.setInt(6, 2);
            preparedStatement.setInt(7, entity.getId());
            preparedStatement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBAccessException();
        } finally {
            MyConnectionPool.returnConnectionToPool(connection);
        }
    }

    @Override
    public void delete(int entityId) throws DBAccessException, ClosingConnectionException {
        try {
            connection = MyConnectionPool.getConnectionFromPool();
            PreparedStatement preparedStatement = connection.prepareStatement(resourceManager.getString("DELETE_HUMAN"));
            preparedStatement.setInt(1, entityId);
            preparedStatement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBAccessException();
        } finally {
            MyConnectionPool.returnConnectionToPool(connection);
        }
    }

    @Override
    public Collection<Human> getAll(Collection<Human> all) throws DBAccessException, ClosingConnectionException {
        all.clear();
        try {
            connection = MyConnectionPool.getConnectionFromPool();
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(resourceManager.getString("SELECT_FROM_HUMAN"));
            HumanBuilder humanBuilder = new HumanBuilder();
            while (rs.next()) {
                humanBuilder.setEmail(rs.getString(4))
                        .setId(rs.getInt(1)).setName(rs.getString(2))
                        .setSurname(rs.getString(3)).setPassword(rs.getString(6))
                        .setPhone(rs.getString(4)).setRoleId(rs.getInt(7));
                all.add(humanBuilder.createHuman());
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBAccessException();
        } finally {
            MyConnectionPool.returnConnectionToPool(connection);
        }
        return all;
    }

    public boolean checkIfEmailExists(String email) throws DBAccessException, ClosingConnectionException {
        try {
            connection = MyConnectionPool.getConnectionFromPool();
            PreparedStatement preparedStatement = connection.prepareStatement(resourceManager.getString("SELECT_FROM_HUMAN_WITH_EMAIL"));
            preparedStatement.setString(1, email);
            ResultSet rs = preparedStatement.executeQuery();
            return rs.next();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBAccessException();
        } finally {
            MyConnectionPool.returnConnectionToPool(connection);
        }
    }

}
