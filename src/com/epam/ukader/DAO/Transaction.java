package com.epam.ukader.DAO;

import com.epam.ukader.bean.Apartment;
import com.epam.ukader.bean.Check;
import com.epam.ukader.bean.Order;
import com.epam.ukader.bean.builder.ApartmentBuilder;
import com.epam.ukader.bean.builder.CheckBuilder;
import com.epam.ukader.bean.builder.OrderBuilder;
import com.epam.ukader.myExeptions.daoException.ClosingConnectionException;
import com.epam.ukader.myExeptions.daoException.DBAccessException;
import com.epam.ukader.service.MyConnectionPool;
import com.epam.ukader.service.ResourceManager;

import java.sql.*;
import java.util.Calendar;

/**
 * Created by Palina Ukadzer on 14.06.2015.
 * Contains required transactions.
 */
public class Transaction {
    private Connection connection;
    private ResourceManager resourceManager = ResourceManager.INSTANCE;

    /**
     * Creates and executes a serializable transaction
     * which creates a check for given order ond apartmentId.
     */
    public void assignApartment(Order order, int apartmentId) throws SQLException, DBAccessException, ClosingConnectionException {
        CheckBuilder checkBuilder = new CheckBuilder();
        OrderBuilder orderBuilder = new OrderBuilder();
        ApartmentBuilder apartmentBuilder = new ApartmentBuilder();
        try {
            connection = MyConnectionPool.getConnectionFromPool();
            connection.setAutoCommit(false);
            connection.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            PreparedStatement statement = connection.prepareStatement(resourceManager.getString("SELECT_FROM_ORDER_ID"));
            statement.setInt(1, order.getId());
            ResultSet rs = statement.executeQuery();
            rs.next();
            Order oldOrder = orderBuilder.setId(rs.getInt(1)).setPeopleNumber(rs.getInt(2))
                    .setHumanId(rs.getInt(3)).setClassId(rs.getInt(4)).setTimeIn(rs.getDate(5))
                    .setTimeOut(rs.getDate(6)).setStatusId(rs.getInt(7)).createOrder();
            if (oldOrder.getStatusId() == 1) {
                PreparedStatement preparedStatement = connection.prepareStatement(resourceManager.getString("SELECT_FROM_APARTMENT_ID"));
                preparedStatement.setInt(1, apartmentId);
                rs = preparedStatement.executeQuery();
                Apartment apartment = new Apartment();
                if(rs.next()) {
                    apartment = apartmentBuilder.setId(rs.getInt(1)).setPeopleNumber(rs.getInt(2))
                            .setPrice(rs.getInt(3)).setClassId(rs.getInt(4)).createApartment();
                }
                int days = (int) (order.getTimeOut().getTime() - order.getTimeIn().getTime()) / 86400000 + 1;
                int price = days * apartment.getPrice();
                Check check = checkBuilder.setApartmentId(apartmentId).setOrderId(order.getId())
                        .setPrice(price).createCheck();
                PreparedStatement preparedStatement1 = connection.prepareStatement(resourceManager.getString("INSERT_INTO_CHECK"));
                preparedStatement1.setInt(1, check.getPrice());
                preparedStatement1.setInt(2, check.getOrderId());
                preparedStatement1.setInt(3, check.getApartmentId());
                preparedStatement1.executeUpdate();
                order.setStatusId(2);
                PreparedStatement preparedStatement2 = connection.prepareStatement(resourceManager.getString("UPDATE_ORDER"));
                preparedStatement2.setInt(1, order.getPeopleNumber());
                preparedStatement2.setInt(2, order.getHumanId());
                preparedStatement2.setInt(3, order.getClassId());
                preparedStatement2.setDate(4, utilToSql(order.getTimeIn()));
                preparedStatement2.setDate(5, utilToSql(order.getTimeOut()));
                preparedStatement2.setInt(6, order.getStatusId());
                preparedStatement2.setInt(7, order.getId());
                preparedStatement2.executeUpdate();
                connection.commit();

            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBAccessException("Transaction failed for assigning apartment");
        } finally {
            connection.setAutoCommit(true);
            MyConnectionPool.returnConnectionToPool(connection);
        }
    }

    /**
     * A transaction for updating order
     */
    public void updateOrder(Order order) throws DBAccessException, ClosingConnectionException, SQLException {
        try {
            connection = MyConnectionPool.getConnectionFromPool();
            connection.setAutoCommit(false);
            connection.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
            PreparedStatement preparedStatementFind = connection.prepareStatement(resourceManager.getString("SELECT_FROM_ORDER_ID"));
            preparedStatementFind.setInt(1, order.getId());
            ResultSet rs = preparedStatementFind.executeQuery();
            rs.next();
            Order thisOrder = new OrderBuilder().setId(rs.getInt(1)).setPeopleNumber(rs.getInt(2))
                    .setHumanId(rs.getInt(3)).setClassId(rs.getInt(4)).setTimeIn(rs.getDate(5))
                    .setTimeOut(rs.getDate(6)).setStatusId(rs.getInt(7)).createOrder();
            if (thisOrder.getStatusId() == 1) {
                PreparedStatement preparedStatement = connection.prepareStatement(resourceManager.getString("UPDATE_ORDER"));
                preparedStatement.setInt(1, order.getPeopleNumber());
                preparedStatement.setInt(2, order.getHumanId());
                preparedStatement.setInt(3, order.getClassId());
                preparedStatement.setDate(4, utilToSql(order.getTimeIn()));
                preparedStatement.setDate(5, utilToSql(order.getTimeOut()));
                preparedStatement.setInt(6, order.getStatusId());
                preparedStatement.setInt(7, order.getId());
                preparedStatement.executeUpdate();
            }
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBAccessException("Transaction failed for updating order");
        } finally {
            connection.setAutoCommit(true);
            MyConnectionPool.returnConnectionToPool(connection);
        }
    }

    private Date utilToSql(java.util.Date date) {
        Calendar cal = Calendar.getInstance();
        java.util.Date utilDate = date; // your util date
        cal.setTime(utilDate);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        Date sqlDate = new Date(cal.getTime().getTime()); // your sql date
        return sqlDate;
    }
}
