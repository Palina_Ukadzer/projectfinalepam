package com.epam.ukader.DAO.entity;

import com.epam.ukader.DAO.ObjectDAO;
import com.epam.ukader.bean.Human;
import com.epam.ukader.myExeptions.daoException.ClosingConnectionException;
import com.epam.ukader.myExeptions.daoException.DBAccessException;

/**
 * Created by Palina Ukadzer on 31.05.2015.
 * Interface for HumanDAO, extended from ObjectDAO
 */
public interface HumanDAO extends ObjectDAO<Human> {
    public Human getByEmailPassword(String email, String password) throws DBAccessException, ClosingConnectionException;

    public boolean checkIfEmailExists(String email) throws DBAccessException, ClosingConnectionException;
}
