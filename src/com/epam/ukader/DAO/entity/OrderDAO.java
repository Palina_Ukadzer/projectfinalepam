package com.epam.ukader.DAO.entity;

import com.epam.ukader.DAO.ObjectDAO;
import com.epam.ukader.bean.Order;
import com.epam.ukader.myExeptions.daoException.ClosingConnectionException;
import com.epam.ukader.myExeptions.daoException.DBAccessException;

import java.util.Collection;

/**
 * Created by Palina Ukadzer on 31.05.2015.
 * Interface for OrderDAO, extended from ObjectDAO
 */
public interface OrderDAO extends ObjectDAO<Order> {

    public Collection<Order> getAllUndoneForId(Collection<Order> all, int id) throws DBAccessException, ClosingConnectionException;

    public Collection<Order> getNotAcceptedOrders(Collection<Order> all) throws DBAccessException, ClosingConnectionException;


}
