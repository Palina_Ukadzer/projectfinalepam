package com.epam.ukader.DAO.entity;

import com.epam.ukader.DAO.ObjectDAO;
import com.epam.ukader.bean.Apartment;

/**
 * Created by Palina Ukadzer on 31.05.2015.
 * Interface for ApartmentDAO, extended from EntityDAO
 */
public interface apartmentDAO extends ObjectDAO<Apartment> {
}
