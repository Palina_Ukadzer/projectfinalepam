package com.epam.ukader.DAO.entity;

import com.epam.ukader.DAO.ObjectDAO;
import com.epam.ukader.bean.ApartmentClass;

/**
 * Created by Palina Ukadzer on 19.06.2015.
 * Interface for ApartmentClassDAO, extended from ObjectDAO
 */
public interface ApartmentClassDAO extends ObjectDAO<ApartmentClass> {
}
