package com.epam.ukader.DAO.entity;

import com.epam.ukader.DAO.ObjectDAO;
import com.epam.ukader.bean.Check;
import com.epam.ukader.myExeptions.daoException.ClosingConnectionException;
import com.epam.ukader.myExeptions.daoException.DBAccessException;

import java.util.Collection;

/**
 * Created by Palina Ukadzer on 31.05.2015.
 * Interface for CheckDAO, extended from ObjectDAO
 */
public interface CheckDAO extends ObjectDAO<Check> {

    Collection<Check> getAllForApartment(Collection<Check> all, int id) throws DBAccessException, ClosingConnectionException;

    Collection<Check> getAllForUser(Collection<Check> all, int id) throws DBAccessException, ClosingConnectionException;


}
