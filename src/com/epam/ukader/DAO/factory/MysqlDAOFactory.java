package com.epam.ukader.DAO.factory;

import com.epam.ukader.DAO.entity.*;
import com.epam.ukader.DAO.implementation.*;

/**
 * Created by Palina Ukadzer on 27.06.2015.
 * Mysql concrete DAO Factory implementation
 */
public class MysqlDAOFactory extends DAOFactory{

    @Override
    public apartmentDAO getApartmentDAO() {
        return new MysqlApartmentDAO();
    }

    @Override
    public ApartmentClassDAO getApartmentClassDAO() {
        return new MysqlApartmentClassDAO();
    }

    @Override
    public OrderDAO getOrderDAO() {
        return new MysqlOrderDAO();
    }

    @Override
    public CheckDAO getCheckDAO() {
        return new MysqlCheckDAO();
    }

    @Override
    public HumanDAO getHumanDAO() {
        return new MysqlHumanDAO();
    }
}
