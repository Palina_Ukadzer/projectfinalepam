package com.epam.ukader.DAO.factory;

import com.epam.ukader.DAO.entity.*;

/**
 * Created by Palina Ukadzer on 27.06.2015.
 * Abstract class DAO Factory
 */
public abstract class DAOFactory {
    // List of DAO types supported by the factory
    public enum SourceName {MYSQL};

    /**
     * There is a method for each DAO that can be
     * created. The concrete factories
     * implement these methods.
     */
    public abstract apartmentDAO getApartmentDAO();
    public abstract ApartmentClassDAO getApartmentClassDAO();
    public abstract OrderDAO getOrderDAO();
    public abstract CheckDAO getCheckDAO();
    public abstract HumanDAO getHumanDAO();

    // Method that gets needed DAOFactory
    public static DAOFactory getDAOFactory(SourceName whichFactory) {
        switch (whichFactory) {
            case MYSQL:
                return new MysqlDAOFactory();
            default:
                return null;
        }
    }


}
