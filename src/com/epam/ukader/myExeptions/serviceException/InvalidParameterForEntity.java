package com.epam.ukader.myExeptions.serviceException;

/**
 * Created by Palina Ukadzer on 15.06.2015.
 * Generated on service layer when parameters are invalid
 */
public class InvalidParameterForEntity extends ServiceException {
    public InvalidParameterForEntity(final String m) {
        super(m);
    }
}
