package com.epam.ukader.myExeptions.serviceException;

/**
 * Created by Palina Ukadzer on 15.06.2015.
 * Main exception from service layer
 */
public class ServiceException extends Exception {
    public ServiceException(final String m) {
        super(m);
    }
}
