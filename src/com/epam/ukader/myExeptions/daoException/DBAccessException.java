package com.epam.ukader.myExeptions.daoException;

/**
 * Created by Palina Ukadzer on 15.06.2015.
 * Thrown when DB couldn't be accessed
 */
public class DBAccessException extends Exception {
    public DBAccessException(final String m) {
        super(m);
    }

    public DBAccessException() {
        super("Cannot access DB");
    }
}
