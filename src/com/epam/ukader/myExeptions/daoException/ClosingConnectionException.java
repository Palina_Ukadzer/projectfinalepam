package com.epam.ukader.myExeptions.daoException;

/**
 * Created by user on 16.06.2015.
 * Generated on DAO layer if connection can not be closed
 */
public class ClosingConnectionException extends Exception {
    public ClosingConnectionException() {
        super("Unable to close connection");
    }
}
