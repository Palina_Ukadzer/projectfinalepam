package com.epam.ukader.myExeptions.controllerException;

/**
 * Created by Palina Ukadzer on 30.06.2015.
 * Generated when parameters are not valid
 */
public class InvalidParameterControllerException extends ControllerException{
    public InvalidParameterControllerException(String m) {
        super(m);
    }
}
