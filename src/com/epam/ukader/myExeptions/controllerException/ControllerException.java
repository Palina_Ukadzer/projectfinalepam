package com.epam.ukader.myExeptions.controllerException;

/**
 * Created by Palina Ukadzer on 15.06.2015.
 * Main exception for controller layer
 */
public class ControllerException extends Exception {
    public ControllerException(final String m) {
        super(m);
    }
}
