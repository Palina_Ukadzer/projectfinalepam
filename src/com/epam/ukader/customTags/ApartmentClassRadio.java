package com.epam.ukader.customTags;

/**
 * Created by Palina Ukadzer on 17.06.2015.
 * Custom tag that creates the inside of a panel of names of classes
 */

import com.epam.ukader.DAO.entity.ApartmentClassDAO;
import com.epam.ukader.DAO.factory.DAOFactory;
import com.epam.ukader.bean.ApartmentClass;
import com.epam.ukader.myExeptions.daoException.ClosingConnectionException;
import com.epam.ukader.myExeptions.daoException.DBAccessException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

@SuppressWarnings("serial")
public class ApartmentClassRadio extends TagSupport {

    //used when classId needs to be chosen
    private int chosenClassID;

    //if panel is blocked attribute should be set to 'true'
    private String isBlocked;


    public void setChosenClassID(int id) {
        this.chosenClassID = id;
    }

    public void setIsBlocked(String isBlocked) {
        this.isBlocked = isBlocked;
    }

    @Override
    public int doStartTag() throws JspException {
        try {
            Locale locale = Locale.getDefault();
            String lang = locale.getLanguage();
            JspWriter out = pageContext.getOut();
            ApartmentClassDAO apartmentClass_dao = DAOFactory.getDAOFactory(DAOFactory.SourceName.MYSQL).getApartmentClassDAO();
            Collection<ApartmentClass> apartmentClasses = new ArrayList<ApartmentClass>();
            apartmentClasses = apartmentClass_dao.getAll(apartmentClasses);
            if (lang.equals("en")) {
                if (chosenClassID == 0) {
                    for (ApartmentClass apartmentClass : apartmentClasses) {
                        out.write("<label class=\"radio-inline\"><input type=\"radio\" name=\"classId\" " +
                                "required=\"required\"\n" +"    value=\"" + apartmentClass.getId() + "\">" +
                                apartmentClass.getName_english() + "</label>");
                    }
                } else {
                    if (isBlocked == null) {
                        for (ApartmentClass apartmentClass : apartmentClasses) {
                            if (chosenClassID == apartmentClass.getId()) {
                                out.write("<label class=\"radio-inline\"><input type=\"radio\" name=\"classId\" " +
                                        "required=\"required\"\n" + "    value=\"" + apartmentClass.getId() +
                                        "\" checked=\"true\">" + apartmentClass.getName_english() + "</label>");
                            } else {
                                out.write("<label class=\"radio-inline\"><input type=\"radio\" name=\"classId\" " +
                                        "required=\"required\"\n" + "    value=\"" + apartmentClass.getId() + "\">" +
                                        apartmentClass.getName_english() + "</label>");
                            }
                        }
                    } else {
                        for (ApartmentClass apartmentClass : apartmentClasses) {
                            if (chosenClassID == apartmentClass.getId()) {
                                out.write("<label class=\"radio-inline\"><input type=\"radio\" name=\"classId\" " +
                                        "required=\"required\"\n" + "    value=\"" + apartmentClass.getId() +
                                        "\" checked=\"true\" disabled>" + apartmentClass.getName_english() + "</label>");
                            } else {
                                out.write("<label class=\"radio-inline\"><input type=\"radio\" name=\"classId\" " +
                                        "required=\"required\"\n" + "    value=\"" + apartmentClass.getId() +
                                        "\" disabled>" + apartmentClass.getName_english() + "</label>");
                            }
                        }
                    }
                }
            } else {
                if (chosenClassID == 0) {
                    for (ApartmentClass apartmentClass : apartmentClasses) {
                        out.write("<label class=\"radio-inline\"><input type=\"radio\" name=\"classId\" " +
                                "required=\"required\"\n" + "    value=\"" + apartmentClass.getId() + "\">" +
                                apartmentClass.getName_russian() + "</label>");
                    }
                } else {
                    if (isBlocked == null) {
                        for (ApartmentClass apartmentClass : apartmentClasses) {
                            if (chosenClassID == apartmentClass.getId()) {
                                out.write("<label class=\"radio-inline\"><input type=\"radio\" name=\"classId\" " +
                                        "required=\"required\"\n" + "    value=\"" + apartmentClass.getId() +
                                        "\" checked=\"true\">" + apartmentClass.getName_russian() + "</label>");
                            } else {
                                out.write("<label class=\"radio-inline\"><input type=\"radio\" name=\"classId\" " +
                                        "required=\"required\"\n" + "    value=\"" + apartmentClass.getId() + "\">" +
                                        apartmentClass.getName_russian() + "</label>");
                            }
                        }
                    } else {
                        for (ApartmentClass apartmentClass : apartmentClasses) {
                            if (chosenClassID == apartmentClass.getId()) {
                                out.write("<label class=\"radio-inline\"><input type=\"radio\" name=\"classId\" " +
                                        "required=\"required\"\n" + "    value=\"" + apartmentClass.getId() +
                                        "\" checked=\"true\" disabled>" + apartmentClass.getName_russian() + "</label>");
                            } else {
                                out.write("<label class=\"radio-inline\"><input type=\"radio\" name=\"classId\" " +
                                        "required=\"required\"\n" + "    value=\"" + apartmentClass.getId() +
                                        "\" disabled>" + apartmentClass.getName_russian() + "</label>");
                            }
                        }
                    }
                }
            }
        } catch (IOException e) {
            throw new JspException(e.getMessage());
        } catch (DBAccessException e) {
            e.printStackTrace();
            throw new JspException(e.getMessage());
        } catch (ClosingConnectionException e) {
            e.printStackTrace();
            throw new JspException(e.getMessage());
        }
        return SKIP_BODY;
    }
}