package com.epam.ukader.customTags;

import com.epam.ukader.DAO.entity.ApartmentClassDAO;
import com.epam.ukader.DAO.factory.DAOFactory;
import com.epam.ukader.bean.ApartmentClass;
import com.epam.ukader.myExeptions.daoException.ClosingConnectionException;
import com.epam.ukader.myExeptions.daoException.DBAccessException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;

/**
 * Created by Palina Ukadzer on 23.06.2015.
 * A custom tag used to fill in the field of table with proper name
 * of class of an apartment in proper language
 */
public class ApartmentClassField extends TagSupport{
    private String classId;

    /**
     * Setter for required attribute 'classId'
     */
    public void setClassId(String classId) {
        this.classId = classId;
    }

    /**
     * The normal doStartTag method
     */
    public int doStartTag() throws JspException {
        try {
            Locale locale = Locale.getDefault();
            String lang = locale.getLanguage();
            JspWriter out = pageContext.getOut();
            ApartmentClassDAO apartmentClass_dao = DAOFactory.getDAOFactory(DAOFactory.SourceName.MYSQL).getApartmentClassDAO();
            Collection<ApartmentClass> apartmentClasses = new ArrayList<ApartmentClass>();
            apartmentClasses = apartmentClass_dao.getAll(apartmentClasses);
            ApartmentClass[] apartmentClassesArray = new ApartmentClass[apartmentClasses.size()];
            int i=0;
            for (ApartmentClass apartmentClass:apartmentClasses){
                apartmentClassesArray[i]=apartmentClass;
                i++;
            }
            if (lang.equals("en")) {
                out.write("<td>" + apartmentClassesArray[Integer.parseInt(classId) - 1].getName_english() + "</td>");
            } else {
                out.write("<td>" + apartmentClassesArray[Integer.parseInt(classId) - 1].getName_russian() + "</td>");
            }

        } catch (DBAccessException e1) {
            e1.printStackTrace();
        } catch (ClosingConnectionException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return SKIP_BODY;
    }
}
