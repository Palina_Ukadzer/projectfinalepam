package com.epam.ukader.bean;

import java.io.Serializable;

/**
 * Created by Palina Ukadzer on 31.05.2015.
 * Human bean. Fields are same as in `human` table in `hotels` db
 */
public class Human implements Serializable {
    private int id;
    private String name;
    private String surname;
    private String email;
    private String phone;
    private transient String password;
    private int roleId;
    private static final long serialVersionUID = 1L;

    Human() {
        id = 0;
        name = "";
        surname = "";
        email = "";
        phone = "";
        password = "";
        roleId = 0;

    }

    public Human(int id, String name, String surname, String email, String phone, String password, int roleId) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.roleId = roleId;

    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getId() {
        return id;
    }

    public String getSurname() {
        return surname;
    }

    public String getPhone() {
        return phone;
    }

    public String getPassword() {
        return password;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public int getRoleId() {
        return roleId;
    }


    @Override
    public String toString() {
        return "human whose id=" + id + "; name=" + name + "; surname=" + surname +
                "; email=" + email + "; phone=" + phone + "; password=" + password + "; role=" + roleId;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Human) {
            Human human = (Human) o;
            if (human.id == this.id) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        return this.id;
    }
}
