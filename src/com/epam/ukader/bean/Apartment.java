package com.epam.ukader.bean;

import java.io.Serializable;

/**
 * Created by Palina Ukadzer on 31.05.2015.
 * Apartment bean. Fields are same as in `apartment` table in `hotels` db
 */
public class Apartment implements Serializable {
    private int id;
    private int peopleNumber;
    private int price;
    private int classId;
    private static final long serialVersionUID = 1L;


    public Apartment() {
        id = 0;
        peopleNumber = 0;
        classId = 0;
        price = 0;
    }

    public Apartment(int id, int peopleNumber, int classId, int price) {
        this.id = id;
        this.peopleNumber = peopleNumber;
        this.classId = classId;
        this.price = price;

    }

    public void setClassId(int classId) {
        this.classId = classId;
    }


    public void setId(int id) {
        this.id = id;
    }

    public void setPeopleNumber(int peopleNumber) {
        this.peopleNumber = peopleNumber;
    }


    public int getPeopleNumber() {
        return peopleNumber;
    }

    public int getId() {
        return id;
    }

    public int getClassId() {
        return classId;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;
    }


    @Override
    public String toString() {
        return "Apartment which id=" + id + "; people number=" + peopleNumber +
                "; price=" + price + "; class Id=" + classId;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Apartment) {
            Apartment apartment = (Apartment) o;
            if (apartment.id == this.id) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        return this.id;
    }
}
