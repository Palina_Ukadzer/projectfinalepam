package com.epam.ukader.bean;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Palina Ukadzer on 31.05.2015.
 * Order bean. Fields are same as in `order` table in `hotels` db
 */
public class Order implements Serializable {
    private int id;
    private int peopleNumber;
    private Date timeIn;
    private Date timeOut;
    private int humanId;
    private int classId;
    private int statusId;
    private static final long serialVersionUID = 1L;

    Order() {
        id = 0;
        peopleNumber = 0;
        timeIn = null;
        timeOut = null;
        humanId = 0;
        classId = 0;
        statusId = 0;
    }

    public Order(int id, int peopleNumber, Date timeIn, Date timeOut, int humanId, int classId, int statusId) {
        this.id = id;
        this.peopleNumber = peopleNumber;
        this.timeIn = timeIn;
        this.timeOut = timeOut;
        this.humanId = humanId;
        this.classId = classId;
        this.statusId = statusId;
    }

    public Date getTimeIn() {
        return timeIn;
    }

    public Date getTimeOut() {
        return timeOut;
    }

    public int getClassId() {
        return classId;
    }

    public int getHumanId() {
        return humanId;
    }

    public int getId() {
        return id;
    }

    public int getStatusId() {
        return statusId;
    }

    public int getPeopleNumber() {
        return peopleNumber;
    }

    public void setClassId(int classId) {
        this.classId = classId;
    }

    public void setHumanId(int humanId) {
        this.humanId = humanId;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setPeopleNumber(int peopleNumber) {
        this.peopleNumber = peopleNumber;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public void setTimeIn(Date timeIn) {
        this.timeIn = timeIn;
    }

    public void setTimeOut(Date timeOut) {
        this.timeOut = timeOut;
    }

    @Override
    public String toString() {
        return "Order id=" + id + "; people number=" + peopleNumber +
                "; time in=" + timeIn + "; time out=" + timeOut +
                "; human id=" + humanId + "; class Id=" +
                classId + "; status id=" + statusId;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Order) {
            Order newOrder = (Order) o;
            if (newOrder.id == this.id) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        return this.id;
    }
}
