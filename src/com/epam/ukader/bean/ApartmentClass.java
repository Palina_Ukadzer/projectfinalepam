package com.epam.ukader.bean;

import java.io.Serializable;

/**
 * Created by Palina Ukadzer on 19.06.2015.
 * ApartmentClass bean. Fields are same as in `class` table in `hotels` db
 */
public class ApartmentClass implements Serializable{

    private int id;
    private String name_russian;
    private String name_english;
    private static final long serialVersionUID = 1L;

    public ApartmentClass(){
        id=0;
        name_english="";
        name_russian="";
    }

    public ApartmentClass(int id,String name_english, String name_russian){
        this.id=id;
        this.name_russian=name_russian;
        this.name_english=name_english;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName_english(String name_english) {
        this.name_english = name_english;
    }

    public void setName_russian(String name_russian) {
        this.name_russian = name_russian;
    }

    public int getId() {
        return id;
    }

    public String getName_russian() {
        return name_russian;
    }

    public String getName_english() {
        return name_english;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof ApartmentClass){
            ApartmentClass newClass=(ApartmentClass) o;
            return id==newClass.id;
        }
        return false;
    }

    @Override
    public String toString() {
        return "Apartment class id="+id+"; english name="+name_english+"; russian name="+name_russian;
    }
}

