package com.epam.ukader.bean;

import java.io.Serializable;

/**
 * Created by Palina Ukadzer on 31.05.2015.
 * Check bean. Fields are same as in `check` table in `hotels` db
 */
public class Check implements Serializable {
    private int id;
    private int price;
    private int orderId;
    private int apartmentId;
    private static final long serialVersionUID = 1L;

    Check() {
        id = 0;
        price = 0;
        orderId = 0;
        apartmentId = 0;
    }

    public Check(int id, int price, int orderId, int apartmentId) {
        this.id = id;
        this.price = price;
        this.orderId = orderId;
        this.apartmentId = apartmentId;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setApartmentId(int apartmentId) {
        this.apartmentId = apartmentId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getApartmentId() {
        return apartmentId;
    }

    public int getId() {
        return id;
    }

    public int getOrderId() {
        return orderId;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "check id=" + id + "; price=" + price + "; order id=" +
                orderId + "; apartment id=" + apartmentId;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Check) {
            Check check = (Check) o;
            if (check.id == this.id) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        return this.id;
    }
}
