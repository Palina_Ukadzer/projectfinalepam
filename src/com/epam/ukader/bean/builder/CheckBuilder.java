package com.epam.ukader.bean.builder;

import com.epam.ukader.bean.Check;

/**
 * Created by Palina Ukadzer
 * Builder for Check
 */
public class CheckBuilder {
    private int id;
    private int price;
    private int orderId;
    private int apartmentId;

    public CheckBuilder setId(int id) {
        this.id = id;
        return this;
    }

    public CheckBuilder setPrice(int price) {
        this.price = price;
        return this;
    }

    public CheckBuilder setOrderId(int orderId) {
        this.orderId = orderId;
        return this;
    }

    public CheckBuilder setApartmentId(int apartmentId) {
        this.apartmentId = apartmentId;
        return this;
    }

    public Check createCheck() {
        return new Check(id, price, orderId, apartmentId);
    }
}