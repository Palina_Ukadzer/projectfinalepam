package com.epam.ukader.bean.builder;

import com.epam.ukader.bean.Apartment;

/**
 * Created by Palina Ukadzer
 * Builder for Apartment
 */
public class ApartmentBuilder {
    private int id;
    private int peopleNumber;
    private int classId;
    private int price;

    public ApartmentBuilder setId(int id) {
        this.id = id;
        return this;
    }

    public ApartmentBuilder setPeopleNumber(int peopleNumber) {
        this.peopleNumber = peopleNumber;
        return this;
    }

    public ApartmentBuilder setClassId(int classId) {
        this.classId = classId;
        return this;
    }

    public ApartmentBuilder setPrice(int price) {
        this.price = price;
        return this;
    }

    public Apartment createApartment() {
        return new Apartment(id, peopleNumber, classId, price);
    }
}