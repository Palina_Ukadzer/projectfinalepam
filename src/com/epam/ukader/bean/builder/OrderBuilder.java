package com.epam.ukader.bean.builder;

import com.epam.ukader.bean.Order;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Palina Ukadzer
 * Builder for Order
 */
public class OrderBuilder {
    private int id;
    private int peopleNumber;
    private Date timeIn;
    private Date timeOut;
    private int humanId;
    private int classId;
    private int statusId;

    public OrderBuilder setId(int id) {
        this.id = id;
        return this;
    }

    public OrderBuilder setPeopleNumber(int peopleNumber) {
        this.peopleNumber = peopleNumber;
        return this;
    }

    public OrderBuilder setTimeIn(Date timeIn) {
        this.timeIn = timeIn;
        return this;
    }

    public OrderBuilder setTimeOut(Date timeOut) {
        this.timeOut = timeOut;
        return this;
    }

    public OrderBuilder setHumanId(int humanId) {
        this.humanId = humanId;
        return this;
    }

    public OrderBuilder setClassId(int classId) {
        this.classId = classId;
        return this;
    }

    public OrderBuilder setStatusId(int statusId) {
        this.statusId = statusId;
        return this;
    }

    public OrderBuilder setTimeIn(String timeIn) {
        Date dateStr = null;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            dateStr = formatter.parse(timeIn);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.timeIn = dateStr;
        return this;
    }

    public OrderBuilder setTimeOut(String timeIn) {
        Date dateStr = null;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            dateStr = formatter.parse(timeIn);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.timeOut = dateStr;
        return this;
    }

    public Order createOrder() {
        return new Order(id, peopleNumber, timeIn, timeOut, humanId, classId, statusId);
    }
}