package com.epam.ukader.bean.builder;

import com.epam.ukader.bean.Human;

/**
 * Created by Palina Ukadzer
 * Builder for Human
 */
public class HumanBuilder {
    private int id;
    private String name;
    private String surname;
    private String email;
    private String phone;
    private String password;
    private int roleId;

    public HumanBuilder setId(int id) {
        this.id = id;
        return this;
    }

    public HumanBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public HumanBuilder setSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public HumanBuilder setEmail(String email) {
        this.email = email;
        return this;
    }

    public HumanBuilder setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public HumanBuilder setPassword(String password) {
        this.password = password;
        return this;
    }

    public HumanBuilder setRoleId(int roleId) {
        this.roleId = roleId;
        return this;
    }

    public Human createHuman() {
        return new Human(id, name, surname, email, phone, password, roleId);
    }
}