package com.epam.ukader.bean.builder;

import com.epam.ukader.bean.ApartmentClass;

/**
 * Created by Palina Ukadzer
 * Builder for ApartmentClass
 */
public class ApartmentClassBuilder {
    private int id;
    private String name_english;
    private String name_russian;

    public ApartmentClassBuilder setId(int id) {
        this.id = id;
        return this;
    }

    public ApartmentClassBuilder setName_english(String name_english) {
        this.name_english = name_english;
        return this;
    }

    public ApartmentClassBuilder setName_russian(String name_russian) {
        this.name_russian = name_russian;
        return this;
    }

    public ApartmentClass createApartmentClass() {
        return new ApartmentClass(id, name_english, name_russian);
    }
}