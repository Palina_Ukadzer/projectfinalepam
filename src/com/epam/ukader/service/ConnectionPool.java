package com.epam.ukader.service;

/**
 * Created by Palina Ukadzer on 13.06.2015.
 * Realises connection pool (Tomcat) (not used but let it stay just in case)
 */

import java.sql.Connection;
import java.sql.SQLException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class ConnectionPool {
    private static ResourceManager resourceManager = ResourceManager.INSTANCE;
    private static final String DATASOURCE_NAME = resourceManager.getString("DATASOURCE_NAME");
    private static DataSource dataSource;

    static {
        try {
            Context initContext = new InitialContext();
            Context envContext = (Context) initContext.lookup(resourceManager.getString("LOOKUP_PLACE"));
            dataSource = (DataSource) envContext.lookup(DATASOURCE_NAME);
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    private ConnectionPool() {
    }

    /**
     * Returns a connection if there are any free
     */
    public static Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }

    /**
     * Returns a connection to the pool
     */
    public static void returnConnection(Connection connection) throws SQLException {
        connection.close();
    }


}