package com.epam.ukader.service;

import com.epam.ukader.DAO.entity.CheckDAO;
import com.epam.ukader.DAO.entity.HumanDAO;
import com.epam.ukader.DAO.entity.OrderDAO;
import com.epam.ukader.DAO.factory.DAOFactory;
import com.epam.ukader.bean.Check;
import com.epam.ukader.bean.Human;
import com.epam.ukader.bean.Order;
import com.epam.ukader.bean.builder.HumanBuilder;
import com.epam.ukader.myExeptions.daoException.ClosingConnectionException;
import com.epam.ukader.myExeptions.daoException.DBAccessException;
import com.epam.ukader.myExeptions.serviceException.InvalidParameterForEntity;
import com.epam.ukader.myExeptions.serviceException.ServiceException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

/**
 * Created by Palina Ukadzer on 02.06.2015.
 * Does operation with information about human
 */
public class UserService {

    private static HumanDAO humanDAO = DAOFactory.getDAOFactory(DAOFactory.SourceName.MYSQL).getHumanDAO();
    private static CheckDAO checkDAO = DAOFactory.getDAOFactory(DAOFactory.SourceName.MYSQL).getCheckDAO();
    private static OrderDAO orderDAO =DAOFactory.getDAOFactory(DAOFactory.SourceName.MYSQL).getOrderDAO();
    private static ResourceManager resourceManager=ResourceManager.INSTANCE;

    /**
     * Returns name of appropriate first page jsp according to
     * given email and password
     */
    public static String findWhereToSend(String email, String password) throws ServiceException {
        String dest;
        Human human;
        if (email == null || password == null) {
            throw new InvalidParameterForEntity("Invalid parameter");
        }
        try {

            human = humanDAO.getByEmailPassword(email, password);

            if (human.getRoleId() == 1) {
                dest = resourceManager.getString("adminFirstPageAddress");
                return dest;
            }
            if (human.getRoleId() == 2) {
                dest = resourceManager.getString("userFirstPageAddress");
                return dest;
            }
            dest = resourceManager.getString("errorPageAddress");

            return dest;
        } catch (DBAccessException e) {
            e.printStackTrace();
            throw new ServiceException("Can not find where to send");
        } catch (ClosingConnectionException e) {
            e.printStackTrace();
            throw new ServiceException("Can not find where to send");
        }
    }

    /**
     * Returns name of appropriate first page jsp according to
     * given id of user
     */
    public static String findWhereToSend(int humanId) throws ServiceException {
        String dest;
        Human human;
        if (humanId < 1) {
            throw new InvalidParameterForEntity("Human Id is less than 1");
        }
        try {
            human = humanDAO.getById(humanId);
        } catch (DBAccessException e) {
            e.printStackTrace();
            throw new ServiceException("Can not find where to send");
        } catch (ClosingConnectionException e) {
            e.printStackTrace();
            throw new ServiceException("Can not find where to send");
        }
        if (human.getRoleId() == 1) {
            dest = resourceManager.getString("adminFirstPageAddress");
            return dest;
        }
        if (human.getRoleId() == 2) {
            dest = resourceManager.getString("userFirstPageAddress");
            return dest;
        }
        dest = resourceManager.getString("errorPageAddress");
        return dest;
    }

    /**
     * Returns collection which contains all order of the user,
     * which status equals 1
     */
    public static Collection<Order> getHumanOrders(Human human) throws ServiceException {
        Collection<Order> orders = new ArrayList<Order>();
        if (human == null) {
            throw new InvalidParameterForEntity("Human is null");
        }
        try {
            orderDAO.getAllUndoneForId(orders, human.getId());
        } catch (DBAccessException e) {
            e.printStackTrace();
            throw new ServiceException("Can not get orders of human");
        } catch (ClosingConnectionException e) {
            e.printStackTrace();
            throw new ServiceException("Can not get orders of human");
        }
        return orders;
    }

    /**
     * Finds human basing on given email and password
     */
    public static Human findHumanByEmailPass(String email, String password) throws ServiceException {
        if (email == null || password == null) {
            throw new InvalidParameterForEntity("Invalid parameter");
        }
        Human human;
        try {
            human = humanDAO.getByEmailPassword(email, password);
        } catch (DBAccessException e) {
            e.printStackTrace();
            throw new ServiceException("Can not find human");
        } catch (ClosingConnectionException e) {
            e.printStackTrace();
            throw new ServiceException("Can not find human");
        }
        return human;
    }

    /**
     * Finds human basing on given id
     */
    public static Human findHumanById(int id) throws ServiceException {
        if (id < 1) {
            throw new InvalidParameterForEntity("Invalid parameter");
        }
        Human human;
        try {
            human = humanDAO.getById(id);
        } catch (DBAccessException e) {
            e.printStackTrace();
            throw new ServiceException("Can not find human");
        } catch (ClosingConnectionException e) {
            e.printStackTrace();
            throw new ServiceException("Can not find human");
        }
        return human;
    }

    /**
     * Adds user, information about whom is in humanInf Map
     */
    public static boolean addUser(Map<String, String[]> humanInf) throws ServiceException {
        if (humanInf.get("name")[0] == null || humanInf.get("surname")[0] == null || humanInf.get("phone")[0] == null ||
                humanInf.get("password")[0] == null || humanInf.get("email")[0] == null) {
            throw new InvalidParameterForEntity("One of parameters is null");
        }
        HumanBuilder humanBuilder = new HumanBuilder();
        Human human = humanBuilder.setName(humanInf.get("name")[0])
                .setSurname(humanInf.get("surname")[0])
                .setPhone(humanInf.get("phone")[0])
                .setPassword(humanInf.get("password")[0])
                .setEmail(humanInf.get("email")[0])
                .setRoleId(1).createHuman();
        boolean exists;
        try {
            exists = humanDAO.checkIfEmailExists(humanInf.get("email")[0]);

            if (exists) {
                return false;
            } else {
                humanDAO.save(human);
                return true;
            }
        } catch (DBAccessException e) {
            e.printStackTrace();
            throw new ServiceException("Can not add user");
        } catch (ClosingConnectionException e) {
            e.printStackTrace();
            throw new ServiceException("Can not add user");
        }
    }

    /**
     * Returns all checks of the user
     */
    public static Collection<Check> getUsersChecks(int humanId) throws ServiceException {
        if (humanId < 1) {
            throw new InvalidParameterForEntity("Human id is less than 1");
        }
        Collection<Check> checks = new ArrayList<Check>();
        try {
            checks = checkDAO.getAllForUser(checks, humanId);
        } catch (DBAccessException e) {
            e.printStackTrace();
            throw new ServiceException("Can not get users check");
        } catch (ClosingConnectionException e) {
            e.printStackTrace();
            throw new ServiceException("Can not get users check");
        }
        return checks;
    }

}
