package com.epam.ukader.service;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * Created by Palina Ukadzer on 20.06.2015.
 * Realises connection pool (mine)
 */
public class MyConnectionPool {
    private static Logger logger = Logger.getLogger(MyConnectionPool.class);
    private static String databaseUrl;
    private static String userName;
    private static String password;
    private static BlockingQueue<Connection> connectionPool;

    /**
     * Basic (and only) constructor
     */
    public MyConnectionPool() {
        ResourceManager resourceManager = ResourceManager.INSTANCE;

        databaseUrl = resourceManager.getString("databaseUrl");
        userName = resourceManager.getString("userName");
        password = resourceManager.getString("password");
        int MAX_POOL_SIZE = Integer.parseInt(resourceManager.getString("maxConnections"));
        connectionPool = new ArrayBlockingQueue<Connection>(MAX_POOL_SIZE);

        for (int i = 0; i < MAX_POOL_SIZE; i++) {
            connectionPool.add(createNewConnectionForPool());
        }

    }

    /**
     * Creates a new connection
     */
    private static Connection createNewConnectionForPool() {
        Connection connection;
        ResourceManager resourceManager1=ResourceManager.INSTANCE;

        try {
            Class.forName(resourceManager1.getString("driver"));
            connection = DriverManager.getConnection(databaseUrl, userName, password);
            logger.info("Connection "+connection+" was created");
        } catch (SQLException sqle) {
            logger.error("Connection couldn't be created (sql exception)");
            return null;
        } catch (ClassNotFoundException cnfe) {
            logger.error("Connection couldn't be created (class not found exception)");;
            return null;
        }

        return connection;
    }

    /**
     * Gets connection from pool if there are any available and null if there are no connections available
     */
    public static Connection getConnectionFromPool() {
        Connection connection = null;

        //Take a connection from pool if possible.
        try {
            connection = connectionPool.take();

            //If connection was deleted, it recreates it
            if (connection == null) {
                connection = createNewConnectionForPool();
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //Giving away the connection from the connection pool
        return connection;
    }

    /**
     * Returns connection to pool
     */
    public static boolean returnConnectionToPool(Connection connection) {
        //Adding the connection from the client back to the connection pool
            return connectionPool.add(connection);
    }
}