package com.epam.ukader.service;

import com.epam.ukader.DAO.Transaction;
import com.epam.ukader.DAO.entity.OrderDAO;
import com.epam.ukader.DAO.factory.DAOFactory;
import com.epam.ukader.bean.Order;
import com.epam.ukader.bean.builder.OrderBuilder;
import com.epam.ukader.myExeptions.daoException.ClosingConnectionException;
import com.epam.ukader.myExeptions.daoException.DBAccessException;
import com.epam.ukader.myExeptions.serviceException.InvalidParameterForEntity;
import com.epam.ukader.myExeptions.serviceException.ServiceException;

import java.sql.SQLException;
import java.util.Date;
import java.util.Map;

/**
 * Created by Palina Ukadzer on 03.06.2015.
 * Receives information about the order,
 * creates thr order and saves it to table `order`
 */
public class OrderService {
    private static OrderDAO order_dao = DAOFactory.getDAOFactory(DAOFactory.SourceName.MYSQL).getOrderDAO();

    /**
     * Updates order using transaction for safety
     */
    private static void update(Order order) throws ServiceException {
        try {

            Transaction transaction = new Transaction();
            transaction.updateOrder(order);

        } catch (DBAccessException e) {
            e.printStackTrace();
            throw new ServiceException("Can not save the order:" + order);
        } catch (ClosingConnectionException e) {
            e.printStackTrace();
            throw new ServiceException("Can not save the order:" + order);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new ServiceException("Can not save the order:" + order);
        }
        return;
    }

    /**
     * Creates a human and saves it to DB
     */
    public static Order get(Map<String, String[]> orderInf, int humanId) throws ServiceException {
        Order order = getInf(orderInf, humanId);
        try {
            receive(order);
        } catch (ServiceException e) {
            e.printStackTrace();
            throw new ServiceException("Cannot save order" + order);
        }
        return order;
    }

    /**
     * Creates and order basing on given information
     */
    private static Order getInf(Map<String, String[]> orderInf, int humanId) throws InvalidParameterForEntity {
        if (orderInf.get("peopleNumber")[0] == null || orderInf.get("timeIn")[0] == null ||
                orderInf.get("timeOut")[0] == null || orderInf.get("timeOut")[0] == null ||
                orderInf.get("classId")[0] == null) {
            throw new InvalidParameterForEntity("one of parameters is null");
        }

        Order order = new OrderBuilder().setPeopleNumber(Integer.parseInt(orderInf.get("peopleNumber")[0]))
                .setHumanId(humanId)
                .setTimeIn(orderInf.get("timeIn")[0])
                .setTimeOut(orderInf.get("timeOut")[0])
                .setClassId(Integer.parseInt(orderInf.get("classId")[0])).setStatusId(1).createOrder();

        if(order.getTimeIn().after(order.getTimeOut()) || order.getTimeIn().before(new Date())){
            throw new InvalidParameterForEntity("wrong time in and/or time out");
        }
        return order;
    }

    /**
     * Saves order to DB
     */
    private static void receive(Order order) throws ServiceException {
        try {
            order_dao.save(order);
        } catch (DBAccessException e) {
            e.printStackTrace();
            throw new ServiceException("Can not save the order:" + order);
        } catch (ClosingConnectionException e) {
            e.printStackTrace();
            throw new ServiceException("Can not save the order:" + order);
        }
        return;
    }


    /**
     * Deletes an order
     */
    public static void delete(int id) throws ServiceException {
        try {
            order_dao.delete(id);
        } catch (DBAccessException e) {
            e.printStackTrace();
            throw new ServiceException("Can not delete order with id:" + id);
        } catch (ClosingConnectionException e) {
            e.printStackTrace();
            throw new ServiceException("Can not delete order with id:" + id);
        }
    }

    /**
     * Gets an order basing on given id
     */
    public static Order getById(int id) throws ServiceException {
        Order order;

        try {
            order = order_dao.getById(id);
        } catch (DBAccessException e) {
            e.printStackTrace();
            throw new ServiceException("Can not get order with id:" + id);
        } catch (ClosingConnectionException e) {
            e.printStackTrace();
            throw new ServiceException("Can not get order with id:" + id);
        }
        return order;
    }

    /**
     * Updates an order
     */
    public static boolean update(Map<String, String[]> orderInf, int humanId) throws ServiceException {

        Order order = getInf(orderInf, humanId);
        order.setId(Integer.parseInt(orderInf.get("orderId")[0]));
        Order thisOrder;

        try {
            thisOrder = order_dao.getById(Integer.parseInt(orderInf.get("orderId")[0]));
            if (thisOrder.getHumanId() == humanId) {
                update(order);
                return true;
            }
        } catch (DBAccessException e) {
            e.printStackTrace();
            throw new ServiceException("Can not update order");
        } catch (ClosingConnectionException e) {
            e.printStackTrace();
            throw new ServiceException("Can not update order");
        }

        return false;
    }
}
