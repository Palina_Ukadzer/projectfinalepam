package com.epam.ukader.service;

import com.epam.ukader.DAO.Transaction;
import com.epam.ukader.DAO.entity.apartmentDAO;
import com.epam.ukader.DAO.entity.CheckDAO;
import com.epam.ukader.DAO.entity.OrderDAO;
import com.epam.ukader.DAO.factory.DAOFactory;
import com.epam.ukader.bean.Apartment;
import com.epam.ukader.bean.Check;
import com.epam.ukader.bean.Order;
import com.epam.ukader.bean.builder.ApartmentBuilder;
import com.epam.ukader.myExeptions.daoException.ClosingConnectionException;
import com.epam.ukader.myExeptions.daoException.DBAccessException;
import com.epam.ukader.myExeptions.serviceException.InvalidParameterForEntity;
import com.epam.ukader.myExeptions.serviceException.ServiceException;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

/**
 * Created by Palina Ukadzer on 05.06.2015.
 * Class provides required methods for actions
 * which can only be done by an administrator
 */
public class AdminService {

    private static apartmentDAO apartment_dao = DAOFactory.getDAOFactory(DAOFactory.SourceName.MYSQL).getApartmentDAO();
    private static OrderDAO order_dao = DAOFactory.getDAOFactory(DAOFactory.SourceName.MYSQL).getOrderDAO();
    private static CheckDAO check_dao = DAOFactory.getDAOFactory(DAOFactory.SourceName.MYSQL).getCheckDAO();

    /**
     * Method finds all orders which status is 'not accepted'
     */
    public static Collection<Order> getNotAcceptedOrders() throws ServiceException {
        Collection<Order> orders = new ArrayList<Order>();

        try {
            orders = order_dao.getNotAcceptedOrders(orders);
        } catch (DBAccessException e) {
            e.printStackTrace();
            throw new ServiceException("Cannot get unaccepted orders");
        } catch (ClosingConnectionException e) {
            e.printStackTrace();
            throw new ServiceException("Cannot get unaccepted orders");
        }
        return orders;
    }

    /**
     * Method selects all apartments, that have characteristics,
     * as specified in the order and are available during time,
     * specified in the order
     */
    public static Collection<Apartment> chooseAvailableApartments(int orderId) throws ServiceException {
        Order order;

        try {
            if (orderId < 1) {
                throw new InvalidParameterForEntity("orderId is not valid");
            }

            order = order_dao.getById(orderId);

            Collection<Apartment> allApartments = new ArrayList<Apartment>();
            Collection<Apartment> availableApartments = new ArrayList<Apartment>();
            allApartments = apartment_dao.getAll(allApartments);
            Collection<Check> checks = new ArrayList<Check>();
            for (Apartment apartment : allApartments) {
                if (apartment.getPeopleNumber() == order.getPeopleNumber() &&
                        apartment.getClassId() == order.getClassId()) {
                    if (isApartmentAvailable(order, apartment.getId())) {
                        availableApartments.add(apartment);
                    }
                }
            }

            return availableApartments;
        } catch (DBAccessException e) {
            e.printStackTrace();
            throw new ServiceException("Can not choose available apartments");
        } catch (ClosingConnectionException e) {
            e.printStackTrace();
            throw new ServiceException("Can not choose available apartments");
        }
    }

    /**
     * Method checks if the apartment have characteristics,
     * as specified in the order and available during time,
     * specified in the order
     */
    private static boolean isApartmentAvailable(Order order, int apartmentId) throws ServiceException {

        Collection<Check> checks = new ArrayList<Check>();
        boolean available = true;

        if (order == null || apartmentId < 1) {
            throw new InvalidParameterForEntity("parameters are not valid");
        }

        try {
            checks = check_dao.getAllForApartment(checks, apartmentId);

            //Checks all scenarios when apartment can be occupied for time given in the order
            for (Check check : checks) {
                int orderId = check.getOrderId();
                Order thisOrder = order_dao.getById(orderId);
                if (order.getTimeIn().before(thisOrder.getTimeOut()) &&
                        order.getTimeOut().after(thisOrder.getTimeOut())) {
                    available = false;
                }
                if (order.getTimeIn().before(thisOrder.getTimeOut()) &&
                        order.getTimeOut().after(thisOrder.getTimeOut())) {
                    available = false;
                }
                if (order.getTimeIn().before(thisOrder.getTimeIn()) &&
                        order.getTimeOut().after(thisOrder.getTimeIn())) {
                    available = false;
                }
                if (order.getTimeIn().before(thisOrder.getTimeIn())
                        && order.getTimeOut().after(thisOrder.getTimeOut())) {
                    available = false;
                }
                if (order.getTimeIn().after(thisOrder.getTimeIn())
                        && order.getTimeOut().before(thisOrder.getTimeOut())) {
                    available = false;
                }
                if (order.getTimeIn().equals(thisOrder.getTimeIn()) ||
                        order.getTimeOut().equals(thisOrder.getTimeOut())) {
                    available = false;
                }
            }
        } catch (DBAccessException e) {
            e.printStackTrace();
            throw new ServiceException("Cannot check if apartment is available");
        } catch (ClosingConnectionException e) {
            e.printStackTrace();
            throw new ServiceException("Cannot check if apartment is available");
        }
        return available;
    }


    /**
     * If apartment is not being processed, method returns true and
     * locks the apartment
     * and if it is, methods returns false
     */
    public static void chooseThisApartment(int orderId, int apartmentId) throws ServiceException {
        Order order;

        if (orderId < 1 || apartmentId < 1) {
            throw new InvalidParameterForEntity("invalid parameter");
        }

        try {
            order = order_dao.getById(orderId);
            Apartment apartment = apartment_dao.getById(apartmentId);
            if (isApartmentAvailable(order, apartmentId)) {
                try {
                    new Transaction().assignApartment(order, apartmentId);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        } catch (DBAccessException e) {
            e.printStackTrace();
            throw new ServiceException("Can not choose apartment");
        } catch (ClosingConnectionException e) {
            e.printStackTrace();
            throw new ServiceException("Can not choose apartment");
        }
    }

    /**
     * Method adds an apartment with specified characteristics
     */
    public static void addApartment(Map<String, String[]> apartmentInf) throws ServiceException {
        if (apartmentInf.get("peopleNumber")[0] == null || apartmentInf.get("classId")[0] == null ||
                apartmentInf.get("price")[0] == null) {
            throw new InvalidParameterForEntity("One of parameters is null");
        }

        Apartment apartment = new ApartmentBuilder().setClassId(Integer.parseInt(apartmentInf.get("classId")[0]))
                .setPeopleNumber(Integer.parseInt(apartmentInf.get("peopleNumber")[0]))
                .setPrice(Integer.parseInt(apartmentInf.get("price")[0])).createApartment();

        try {
            apartment_dao.save(apartment);
        } catch (DBAccessException e) {
            e.printStackTrace();
            throw new ServiceException("Can not add apartment");
        } catch (ClosingConnectionException e) {
            e.printStackTrace();
            throw new ServiceException("Can not add apartment");
        }
    }
}
