package com.epam.ukader.contraller;

import com.epam.ukader.contraller.command.*;

import javax.servlet.http.HttpSession;
import java.util.HashMap;

/**
 * Created by Palina Ukadzer on 13.06.2015.
 * Eases access to commands
 */
public class RequestHelper {
    private static RequestHelper instance = null;
    private HashMap<String, Command> commands = new HashMap<String, Command>();


    /**
     * Fills 'commands' HashMap with name of uri as a 'key' parameter
     * and command which should be executed for given uri as 'value' parameter
     */
    public RequestHelper() {
        super();
        commands.put("LogIn", new LogInCommand());
        commands.put("LogOut", new LogOutCommand());
        commands.put("ChangeOrder", new ChangeOrderCommand());
        commands.put("ChooseThisApartment", new ChooseApartmentCommand());
        commands.put("DeleteOrder", new DeleteOrderCommand());
        commands.put("AddUser", new AddUserCommand());
        commands.put("NewOrder", new NewOrderCommand());
        commands.put("ProcessOrder", new ProcessOrderCommand());
        commands.put("Register", new RegisterCommand());
        commands.put("SeeChecks", new SeeChecksCommand());
        commands.put("SeeOrder", new SeeOrderCommand());
        commands.put("TakeOrder", new TakeOrderCommand());
        commands.put("UpdateOrder", new UpdateOrderCommand());
        commands.put("AddApartment", new AddApartmentCommand());
        commands.put("CreateApartment", new CreateApartmentCommand());

    }

    /**
     * Returns instance
     */
    public static RequestHelper getInstance() {
        if (instance == null) {
            instance = new RequestHelper();
        }
        return instance;
    }

    /**
     * Checks if access for current user is allowed and if so
     * returns the command, that is to be executed for incoming uri
     */
    public Command getCommand(String commandName, HttpSession session) {
        Command command;
        command = commands.get(commandName);
        if (command instanceof LogInCommand || command instanceof LogOutCommand ||
                command instanceof RegisterCommand || command instanceof AddUserCommand) {
            if (session.getAttribute("orderIdForProcessing") != null) {
                session.setAttribute("orderIdForProcessing", null);
            }
            return command;
        }
        if (command instanceof UpdateOrderCommand || command instanceof NewOrderCommand ||
                command instanceof SeeChecksCommand || command instanceof SeeOrderCommand ||
                command instanceof TakeOrderCommand ) {
            if (session.getAttribute("orderIdForProcessing") != null) {
                session.setAttribute("orderIdForProcessing", null);
            }
            if (session.getAttribute("role").toString().equals("user")) {
                return command;
            } else {
                return new NotAllowedCommand();
            }
        }
        if (command instanceof AddApartmentCommand || command instanceof CreateApartmentCommand) {
            if (session.getAttribute("orderIdForProcessing") != null) {
                session.setAttribute("orderIdForProcessing", null);
            }
            if (session.getAttribute("role").toString().equals("admin")) {
                return command;
            } else {
                return new NotAllowedCommand();
            }
        }
        if (command instanceof ChangeOrderCommand) {
            if (session.getAttribute("role").toString().equals("user")) {
                return command;
            } else {
                return new NotAllowedCommand();
            }
        }
        if (command instanceof ChooseApartmentCommand || command instanceof ProcessOrderCommand) {
            if (session.getAttribute("role").toString().equals("admin")) {
                return command;
            } else {
                return new NotAllowedCommand();
            }
        }
        if (command instanceof DeleteOrderCommand) {
            if (session.getAttribute("orderIdForProcessing") != null) {
                session.setAttribute("orderIdForProcessing", null);
            }
            if (session.getAttribute("humanId") != null) {
                return command;
            } else {
                return new NotAllowedCommand();
            }
        }
        return new NotAllowedCommand();

    }
}