package com.epam.ukader.contraller.listener;

import org.apache.log4j.Logger;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;

/**
 * Created by Palina Ukadzer on 15.06.2015.
 * Listens to events of the session
 */
@WebListener
public class SessionListener implements HttpSessionAttributeListener {

    private Logger logger = Logger.getLogger(this.getClass());

    /**
     * Writes to log file name and value of removed attribute
     */
    public void attributeRemoved(HttpSessionBindingEvent ev) {
        logger.info("remove: " + ev.getClass().getSimpleName() + " : " + ev.getName()
                + " : " + ev.getValue());
    }

    /**
     * Writes to log file name and value of added attribute
     */
    public void attributeAdded(HttpSessionBindingEvent ev) {
        logger.info("add: " + ev.getClass().getSimpleName() + " : " + ev.getName()
                + " : " + ev.getValue());
    }

    /**
     * Writes to log file name and value of replaced attribute
     */
    public void attributeReplaced(HttpSessionBindingEvent ev) {
        logger.info("replace: " + ev.getClass().getSimpleName() + " : " + ev.getName()
                + " : " + ev.getValue());
    }
}