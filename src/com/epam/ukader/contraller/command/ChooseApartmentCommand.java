package com.epam.ukader.contraller.command;

import com.epam.ukader.bean.Order;
import com.epam.ukader.myExeptions.controllerException.ControllerException;
import com.epam.ukader.myExeptions.serviceException.ServiceException;
import com.epam.ukader.service.AdminService;
import com.epam.ukader.service.ResourceManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Collection;

/**
 * Created by Palina Ukadzer on 13.06.2015.
 * Command for ChooseThisApartment
 */
public class ChooseApartmentCommand implements Command {

    private Logger logger = Logger.getLogger(this.getClass());
    private ResourceManager resourceManager=ResourceManager.INSTANCE;

    /**
     * If an order can be processed method processes it
     */
    @Override
    public String execute(HttpServletRequest request, HttpSession session) throws ControllerException {
        if (request.getParameter("language") == null) {
            if (session.getAttribute("orderIdForProcessing") != null) {
                try {
                    AdminService.chooseThisApartment((Integer) session.getAttribute("orderIdForProcessing"),
                            Integer.parseInt(request.getParameter("apartmentId")));
                } catch (ServiceException e) {
                    e.printStackTrace();
                    throw new ControllerException("Can not choose apartment");
                }
            }
            Collection<Order> orders;
            try {
                orders = AdminService.getNotAcceptedOrders();
            } catch (ServiceException e) {
                e.printStackTrace();
                logger.error("Can not choose apartment");
                throw new ControllerException("Can not choose apartment");
            }
            session.setAttribute("orderIdForProcessing", null);
            request.setAttribute("orders", orders);
            return resourceManager.getString("adminFirstPageAddress");
        } else {
            Collection<Order> orders;
            try {
                orders = AdminService.getNotAcceptedOrders();
            } catch (ServiceException e) {
                e.printStackTrace();
                logger.error("Can not choose apartment");
                throw new ControllerException("Can not choose apartment");
            }
            request.setAttribute("orders", orders);
            return resourceManager.getString("adminFirstPageAddress");
        }

    }
}
