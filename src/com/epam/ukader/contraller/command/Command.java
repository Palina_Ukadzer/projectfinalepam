package com.epam.ukader.contraller.command;

import com.epam.ukader.myExeptions.controllerException.ControllerException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by Palina Ukadzer on 13.06.2015.
 * Interface for command pattern
 */
public interface Command {
    String NO_RESULT = "error.jsp";
    Command NO_ACTION = new Command()

    {
        @Override
        public String execute(HttpServletRequest request, HttpSession session) {
            return NO_RESULT;
        }

    };

    String execute(HttpServletRequest request, HttpSession session) throws ControllerException;
}
