package com.epam.ukader.contraller.command;

import com.epam.ukader.bean.Order;
import com.epam.ukader.myExeptions.controllerException.ControllerException;
import com.epam.ukader.myExeptions.serviceException.ServiceException;
import com.epam.ukader.service.OrderService;
import com.epam.ukader.service.ResourceManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by Palina Ukadzer on 13.06.2015.
 * Command for SeeOrder
 */
public class SeeOrderCommand implements Command {

    private Logger logger = Logger.getLogger(this.getClass());
    private ResourceManager resourceManager=ResourceManager.INSTANCE;

    /**
     * Finds order by id and returns seeOrder.jsp
     */
    @Override
    public String execute(HttpServletRequest request, HttpSession session) throws ControllerException {
        Order order;
        if (request.getParameter("language") == null) {
            session.setAttribute("orderToSeeId", Integer.parseInt(request.getParameter("orderId")));
        }
        try {
            order = OrderService.getById(Integer.parseInt(session.getAttribute("orderToSeeId").toString()));

            request.setAttribute("order", order);
            return resourceManager.getString("seeOrderPageAddress");
        } catch (ServiceException e) {
            e.printStackTrace();
            logger.error("Can not see order");
            throw new ControllerException("Can not see order");

        }
    }
}
