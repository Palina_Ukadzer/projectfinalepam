package com.epam.ukader.contraller.command;

import com.epam.ukader.bean.Apartment;
import com.epam.ukader.myExeptions.controllerException.ControllerException;
import com.epam.ukader.myExeptions.serviceException.ServiceException;
import com.epam.ukader.service.AdminService;
import com.epam.ukader.service.ResourceManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Collection;

/**
 * Created by Palina Ukadzer on 13.06.2015.
 * Command for ProcessOrder
 */
public class ProcessOrderCommand implements Command {

    private Logger logger = Logger.getLogger(this.getClass());
    private ResourceManager resourceManager=ResourceManager.INSTANCE;

    /**
     * find apartments available for given order and returns choosing apartment jsp
     */
    @Override
    public String execute(HttpServletRequest request, HttpSession session) throws ControllerException {
        try {
            if (request.getParameter("language") == null) {
                session.setAttribute("orderIdForProcessing", Integer.parseInt(request.getParameter("orderId")));
                Collection<Apartment> apartments = AdminService.chooseAvailableApartments(Integer.parseInt(request.getParameter("orderId")));
                request.setAttribute("orderId", request.getParameter("orderId"));
                request.setAttribute("apartments", apartments);
                return resourceManager.getString("choosingApartmentPageAddress");

            } else {
                Collection<Apartment> apartments = AdminService.chooseAvailableApartments((Integer) session.getAttribute("orderIdForProcessing"));
                request.setAttribute("orderId", session.getAttribute("orderIdForProcessing"));
                request.setAttribute("apartments", apartments);
                return resourceManager.getString("choosingApartmentPageAddress");
            }
        } catch (ServiceException e) {
            e.printStackTrace();
            logger.error("Can not process order");
            throw new ControllerException("Can not process order");
        }

    }
}
