package com.epam.ukader.contraller.command;

import com.epam.ukader.bean.Human;
import com.epam.ukader.bean.Order;
import com.epam.ukader.myExeptions.controllerException.ControllerException;
import com.epam.ukader.myExeptions.serviceException.ServiceException;
import com.epam.ukader.service.OrderService;
import com.epam.ukader.service.ResourceManager;
import com.epam.ukader.service.UserService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Collection;
import java.util.Map;

/**
 * Created by Palina Ukadzer on 13.06.2015.
 * Command for TakeOrder
 */
public class TakeOrderCommand implements Command {

    private Logger logger = Logger.getLogger(this.getClass());
    private ResourceManager resourceManager=ResourceManager.INSTANCE;

    /**
     * Saves the order and return first page name jsp
     */
    @Override
    public String execute(HttpServletRequest request, HttpSession session) throws ControllerException {
        String dest;
        try {
            Map<String, String[]> orderInf = request.getParameterMap();
            if (orderInf.size() > 1) {
                OrderService.get(orderInf, (Integer) session.getAttribute("humanId"));
                Human human = UserService.findHumanById((Integer) session.getAttribute("humanId"));
                Collection<Order> orders = UserService.getHumanOrders(human);
                request.setAttribute("myOrders", orders);
                dest = resourceManager.getString("userFirstPageAddress");
            } else {
                Human human = UserService.findHumanById((Integer) session.getAttribute("humanId"));
                Collection<Order> orders = UserService.getHumanOrders(human);
                request.setAttribute("myOrders", orders);
                dest = resourceManager.getString("userFirstPageAddress");
            }
            return dest;
        }catch (ServiceException e) {
            e.printStackTrace();
            logger.error("Can not take order");
            throw new ControllerException("Can not take order");
        }
    }
}
