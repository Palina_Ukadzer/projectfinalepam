package com.epam.ukader.contraller.command;

import com.epam.ukader.service.ResourceManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Locale;

/**
 * Created by Palina Ukadzer on 13.06.2015.
 * Command for LogOut
 */
public class LogOutCommand implements Command {

    private Logger logger = Logger.getLogger(this.getClass());
    private ResourceManager resourceManager=ResourceManager.INSTANCE;

    /**
     * Invalidates the session and return name of welcome page jsp
     */
    @Override
    public String execute(HttpServletRequest request, HttpSession session) {

        if(request.getParameter("language")!=null){
            return resourceManager.getString("welcomePageAddress");
        }else {
            Locale.setDefault(new Locale("ru"));
        }
        logger.info("User logged out");
        session.removeAttribute("humanId");
        session.invalidate();
        return resourceManager.getString("welcomePageAddress");
    }
}
