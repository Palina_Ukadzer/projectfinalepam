package com.epam.ukader.contraller.command;

import com.epam.ukader.myExeptions.controllerException.ControllerException;
import com.epam.ukader.myExeptions.serviceException.ServiceException;
import com.epam.ukader.service.ResourceManager;
import com.epam.ukader.service.UserService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * Created by Palina Ukadzer on 13.06.2015.
 * Command for AddUser
 */
public class AddUserCommand implements Command {

    private Logger logger = Logger.getLogger(this.getClass());
    private ResourceManager resourceManager=ResourceManager.INSTANCE;
    /**
     * Adds a user
     */
    @Override
    public String execute(HttpServletRequest request, HttpSession session) throws ControllerException {
        Map<String, String[]> humanInf = request.getParameterMap();
        boolean added;
        try {
            added = UserService.addUser(humanInf);
        } catch (ServiceException e) {
            e.printStackTrace();
            logger.error("Can not add user");
            throw new ControllerException("Can not add user");
        }
        if (added) {
            return resourceManager.getString("youAreRegisteredPageAddress");
        } else {
            return resourceManager.getString("notRegisteredPageAddress");
        }
    }
}
