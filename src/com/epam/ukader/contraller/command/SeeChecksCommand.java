package com.epam.ukader.contraller.command;

import com.epam.ukader.bean.Check;
import com.epam.ukader.myExeptions.controllerException.ControllerException;
import com.epam.ukader.myExeptions.serviceException.ServiceException;
import com.epam.ukader.service.ResourceManager;
import com.epam.ukader.service.UserService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Collection;

/**
 * Created by Palina Ukadzer on 13.06.2015.
 * Command for SeeChecks
 */
public class SeeChecksCommand implements Command {

    private Logger logger = Logger.getLogger(this.getClass());
    private ResourceManager resourceManager=ResourceManager.INSTANCE;

    /**
     * Gets checks of given user and returns checks.jsp
     */
    @Override
    public String execute(HttpServletRequest request, HttpSession session) throws ControllerException {
        Collection<Check> myChecks;
        try {
            myChecks = UserService.getUsersChecks((Integer) session.getAttribute("humanId"));
        } catch (ServiceException e) {
            e.printStackTrace();
            logger.error("Can not see checks");
            throw new ControllerException("Can not see checks");
        }
        request.setAttribute("checks", myChecks);
        return resourceManager.getString("checksPageAddress");
    }
}
