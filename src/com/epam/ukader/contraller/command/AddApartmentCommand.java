package com.epam.ukader.contraller.command;

import com.epam.ukader.myExeptions.controllerException.ControllerException;
import com.epam.ukader.service.ResourceManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by Palina Ukadzer on 20.06.2015.
 * Command for AddApartment
 */
public class AddApartmentCommand implements Command{

    private ResourceManager resourceManager=ResourceManager.INSTANCE;

    /**
     * Returns name of the page of new apartment former
     */
    @Override
    public String execute(HttpServletRequest request, HttpSession session) throws ControllerException {
        return resourceManager.getString("newApartmentPageAddress");
    }
}
