package com.epam.ukader.contraller.command;

import com.epam.ukader.bean.Order;
import com.epam.ukader.myExeptions.controllerException.ControllerException;
import com.epam.ukader.myExeptions.serviceException.ServiceException;
import com.epam.ukader.service.AdminService;
import com.epam.ukader.service.ResourceManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Collection;
import java.util.Map;

/**
 * Created by Palina Ukadzer on 20.06.2015.
 * Command for CreateApartment
 */
public class CreateApartmentCommand implements Command{

    private ResourceManager resourceManager=ResourceManager.INSTANCE;
    private Logger logger = Logger.getLogger(this.getClass());

    /**
     * Simply adds an apartment to db
     */
    @Override
    public String execute(HttpServletRequest request, HttpSession session) throws ControllerException {
                Map<String, String[]> apartmentInf = request.getParameterMap();
        try {
            if(apartmentInf.size()>2) {
                AdminService.addApartment(apartmentInf);
                Collection<Order> orders = AdminService.getNotAcceptedOrders();
                request.setAttribute("orders", orders);
                return resourceManager.getString("adminFirstPageAddress");
            }else {
                return resourceManager.getString("newApartmentPageAddress");
            }
        }catch (ServiceException e) {
            e.printStackTrace();
            logger.error("Can not add apartment");
            throw new ControllerException("Can not add apartment");
        }
    }
}
