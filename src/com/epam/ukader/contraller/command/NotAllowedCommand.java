package com.epam.ukader.contraller.command;

import com.epam.ukader.service.ResourceManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by Palina Ukadzer on 13.06.2015.
 * Command for NotAllowed
 */
public class NotAllowedCommand implements Command {

    private ResourceManager resourceManager=ResourceManager.INSTANCE;

    /**
     * returns name of notAllowed page jsp
     */
    @Override
    public String execute(HttpServletRequest request, HttpSession session) {
        return resourceManager.getString("notAllowedPageAddress");
    }
}
