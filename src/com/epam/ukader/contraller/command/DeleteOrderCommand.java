package com.epam.ukader.contraller.command;

import com.epam.ukader.bean.Human;
import com.epam.ukader.bean.Order;
import com.epam.ukader.myExeptions.controllerException.ControllerException;
import com.epam.ukader.myExeptions.serviceException.ServiceException;
import com.epam.ukader.service.AdminService;
import com.epam.ukader.service.OrderService;
import com.epam.ukader.service.ResourceManager;
import com.epam.ukader.service.UserService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Collection;

/**
 * Created by Palina Ukadzer on 13.06.2015.
 * Command for DeleteOrder
 */
public class DeleteOrderCommand implements Command {

    private Logger logger = Logger.getLogger(this.getClass());
    private ResourceManager resourceManager=ResourceManager.INSTANCE;

    /**
     * Deletes chosen order and returns name of the required for current user jsp
     */
    @Override
    public String execute(HttpServletRequest request, HttpSession session) throws ControllerException {
        if (request.getParameter("orderId") != null) {
            try {
                OrderService.delete(Integer.parseInt(request.getParameter("orderId")));

                Human human = UserService.findHumanById((Integer) session.getAttribute("humanId"));
                if (human.getRoleId() == 2) {
                    Collection<Order> orders = UserService.getHumanOrders(human);
                    request.setAttribute("myOrders", orders);
                    return resourceManager.getString("userFirstPageAddress");
                }
                if (human.getRoleId() == 1) {
                    Collection<Order> orders = AdminService.getNotAcceptedOrders();
                    request.setAttribute("orders", orders);
                    return resourceManager.getString("adminFirstPageAddress");
                }
            } catch (ServiceException e) {
                e.printStackTrace();
                logger.error("Can not delete order");
                throw new ControllerException("Can not delete order");
            }
        } else {
            Human human;
            try {
                human = UserService.findHumanById((Integer) session.getAttribute("humanId"));

                if (human.getRoleId() == 2) {
                    Collection<Order> orders = UserService.getHumanOrders(human);
                    request.setAttribute("myOrders", orders);
                    return resourceManager.getString("userFirstPageAddress");
                }
                if (human.getRoleId() == 1) {
                    Collection<Order> orders = AdminService.getNotAcceptedOrders();
                    request.setAttribute("orders", orders);
                    return resourceManager.getString("adminFirstPageAddress");
                }
            } catch (ServiceException e) {
                e.printStackTrace();
                logger.error("Can not delete order");
                throw new ControllerException("Can not delete order");
            }
        }
        return resourceManager.getString("notAllowedPageAddress");
    }
}
