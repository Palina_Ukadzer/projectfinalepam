package com.epam.ukader.contraller.command;

import com.epam.ukader.service.ResourceManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by Palina Ukadzer on 13.06.2015.
 * Command for Register
 */
public class RegisterCommand implements Command {

    private ResourceManager resourceManager=ResourceManager.INSTANCE;

    /**
     * gets Session and returns to registration jsp
     */
    @Override
    public String execute(HttpServletRequest request, HttpSession session) {
        session = request.getSession(true);
        return resourceManager.getString("registrationPageAddress");
    }
}
