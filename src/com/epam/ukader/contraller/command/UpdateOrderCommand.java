package com.epam.ukader.contraller.command;

import com.epam.ukader.bean.Human;
import com.epam.ukader.bean.Order;
import com.epam.ukader.myExeptions.controllerException.ControllerException;
import com.epam.ukader.myExeptions.serviceException.ServiceException;
import com.epam.ukader.service.OrderService;
import com.epam.ukader.service.ResourceManager;
import com.epam.ukader.service.UserService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Collection;
import java.util.Map;

/**
 * Created by Palina Ukadzer on 13.06.2015.
 * Command for UpdateOrder
 */
public class UpdateOrderCommand implements Command {

    private Logger logger = Logger.getLogger(this.getClass());
    private ResourceManager resourceManager=ResourceManager.INSTANCE;

    /**
     * Updates an order
     */
    @Override
    public String execute(HttpServletRequest request, HttpSession session) throws ControllerException {
        try {

            if (session.getAttribute("updated").toString().equals("yes")) {
                Human human = UserService.findHumanById((Integer) session.getAttribute("humanId"));
                Collection<Order> orders = UserService.getHumanOrders(human);
                request.setAttribute("myOrders", orders);
                return "LogIn";
            }
            session.setAttribute("updated", "yes");
            Map<String, String[]> orderInf = request.getParameterMap();
            if (OrderService.update(orderInf, (Integer) session.getAttribute("humanId"))) {
                Human human = UserService.findHumanById((Integer) session.getAttribute("humanId"));
                Collection<Order> orders = UserService.getHumanOrders(human);
                request.setAttribute("myOrders", orders);
                return resourceManager.getString("userFirstPageAddress");
            } else {
                return "/WEB-INF/notAllowed.jsp";
            }
        } catch (ServiceException e) {
            e.printStackTrace();
            logger.error("Can not update order");
            throw new ControllerException("Can not update order");
        }
    }
}
