package com.epam.ukader.contraller.command;

import com.epam.ukader.bean.Order;
import com.epam.ukader.myExeptions.controllerException.ControllerException;
import com.epam.ukader.myExeptions.serviceException.ServiceException;
import com.epam.ukader.service.OrderService;
import com.epam.ukader.service.ResourceManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by Palina Ukadzer on 13.06.2015.
 * Command for ChangeOrder
 */
public class ChangeOrderCommand implements Command {

    private Logger logger = Logger.getLogger(this.getClass());
    private ResourceManager resourceManager=ResourceManager.INSTANCE;
    /**
     * Sets session attribute 'updated' to 'no' for security
     * and returns name of jsp page where parameters should be forward to
     */
    @Override
    public String execute(HttpServletRequest request, HttpSession session) throws ControllerException {
        if (request.getParameter("language") == null) {
            session.setAttribute("orderIdForProcessing", request.getParameter("orderId"));
        }
        session.setAttribute("updated", "no");
        Order order;
        try {
            order = OrderService.getById(Integer.parseInt(session.getAttribute("orderIdForProcessing").toString()));
        } catch (ServiceException e) {
            e.printStackTrace();
            logger.error("Cannot change order");
            throw new ControllerException("Can not change order");
        }
        request.setAttribute("order", order);
        return resourceManager.getString("changeOrderPageAddress");

    }
}

