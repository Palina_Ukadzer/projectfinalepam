package com.epam.ukader.contraller.command;

import com.epam.ukader.bean.Human;
import com.epam.ukader.bean.Order;
import com.epam.ukader.myExeptions.controllerException.ControllerException;
import com.epam.ukader.myExeptions.serviceException.ServiceException;
import com.epam.ukader.service.AdminService;
import com.epam.ukader.service.ResourceManager;
import com.epam.ukader.service.UserService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Collection;

/**
 * Created by Palina Ukadzer on 13.06.2015.
 * Command for LogIn
 */
public class LogInCommand implements Command {

    private Logger logger = Logger.getLogger(this.getClass());
    private ResourceManager resourceManager=ResourceManager.INSTANCE;

    /**
     * If user is not logged in yet method logs him in and returns appropriate first page.
     * Otherwise- simply returns appropriate for logged in user first page
     */
    @Override
    public String execute(HttpServletRequest request, HttpSession session) throws ControllerException {
        session = request.getSession(true);
        String dest;
        Human human;
        try {
            if (session.getAttribute("humanId") != null) {
                dest = UserService.findWhereToSend((Integer) session.getAttribute("humanId"));
                human = UserService.findHumanById((Integer) session.getAttribute("humanId"));
            } else {
                dest = UserService.findWhereToSend(request.getParameter("Email"), request.getParameter("Password"));
                human = UserService.findHumanByEmailPass(request.getParameter("Email"), request.getParameter("Password"));
                session.setAttribute("humanId", human.getId());
            }
            request.setAttribute("humanId", session.getAttribute("humanId"));
            if (dest.equals(resourceManager.getString("userFirstPageAddress"))) {
                session.setAttribute("role", "user");
                Collection<Order> orders = UserService.getHumanOrders(human);
                request.setAttribute("myOrders", orders);
                return dest;
            } else {
                if (dest.equals(resourceManager.getString("adminFirstPageAddress"))) {
                    session.setAttribute("role", "admin");
                    Collection<Order> orders = AdminService.getNotAcceptedOrders();
                    request.setAttribute("orders", orders);
                    return dest;
                } else {
                    session.invalidate();
                }
            }
        } catch (ServiceException e) {
            e.printStackTrace();
            logger.error("Can not log in");
            throw new ControllerException("Can not Log in");
        }
        return dest;
    }
}
