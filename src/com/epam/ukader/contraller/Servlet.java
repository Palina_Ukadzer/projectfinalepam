package com.epam.ukader.contraller;

import com.epam.ukader.contraller.command.Command;
import com.epam.ukader.myExeptions.controllerException.ControllerException;
import com.epam.ukader.service.MyConnectionPool;
import com.epam.ukader.service.ResourceManager;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.Locale;

/**
 * Created by Palina Ukadzer on 31.05.2015.
 * Normal servlet
 */
@WebServlet("/")
public class Servlet extends HttpServlet {

    private RequestHelper requestHelper = new RequestHelper();
    private Logger logger = Logger.getLogger(this.getClass());
    private ResourceManager resourceManager=ResourceManager.INSTANCE;

    /**
     * Initialisation
     */
    @Override
    public void init() throws ServletException {
        super.init();
        new MyConnectionPool();
        PropertyConfigurator.configure("C:/Users/user/Documents/IdeaProjects/ProjectFinalEPAM/src/com/epam/ukader/log4j.properties");
    }

    /**
     * doGet method
     */
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    /**
     * doPost method
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    /**
     * Processes doGet and doPost requests
     */
    protected void processRequest(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        String uri;

        try {
                HttpSession session = req.getSession(true);

            if(req.getParameter("language")!=null){
                Locale.setDefault(Locale.forLanguageTag(req.getParameter("language")));
            }
            uri = req.getParameter("command");
            if(req.getParameter("language")!=null && uri==null){
                RequestDispatcher dispatcher = req.getRequestDispatcher(resourceManager.getString("welcomePageAddress"));
                dispatcher.forward(req, resp);
            }
            if (uri == null) {
                uri = req.getRequestURI();
            }
            if (uri.equals("/hi")) {
                RequestDispatcher dispatcher = req.getRequestDispatcher(resourceManager.getString("welcomePageAddress"));
                dispatcher.forward(req, resp);
            }
            Command command = requestHelper.getCommand(uri, session);
            String where = null;
            try {
                where = command.execute(req, session);
            } catch (ControllerException e) {
                e.printStackTrace();
                RequestDispatcher dispatcher = req.getRequestDispatcher(resourceManager.getString("errorPageAddress"));
                dispatcher.forward(req, resp);
            }
            if (where != null) {
                RequestDispatcher dispatcher = req.getRequestDispatcher(where);
                dispatcher.forward(req, resp);
            }else {

                RequestDispatcher dispatcher = req.getRequestDispatcher(resourceManager.getString("errorPageAddress"));
                dispatcher.forward(req, resp);
            }
        } catch (Exception e) {
            logger.error("Something is seriously wrong");
            RequestDispatcher dispatcher = req.getRequestDispatcher(resourceManager.getString("errorPageAddress"));
            dispatcher.forward(req, resp);
        }
    }
}

