package com.epam.ukader.contraller.filter;

/**
 * Created by Palina Ukadzer on 30.06.2015.
 * Checks if dates are valid
 */

import com.epam.ukader.service.ResourceManager;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;

@WebFilter(urlPatterns = {"/"})
public class DateFilter implements Filter {


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        String dayFrom = request.getParameter("timeIn");
        String dayTo = request.getParameter("timeOut");
        Date firstDay = null;
        Date lastDay = null;
        ResourceManager resourceManager = ResourceManager.INSTANCE;

        if (dayFrom != null && dayTo != null) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            try {
                firstDay = formatter.parse(dayFrom);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            try {
                lastDay = formatter.parse(dayTo);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        if (firstDay != null && lastDay != null) {
            if (!firstDay.after(lastDay) && !firstDay.before(new Date())) {
                chain.doFilter(request, response);
            } else {
                RequestDispatcher dispatcher = request.getRequestDispatcher(resourceManager.getString("invalidParameterPageAddress"));
                dispatcher.forward(request, response);
            }
        }else {
            chain.doFilter(request, response);
        }
    }

    /**
     * Destroys private parameters
     */
    public void destroy() {
    }
}
