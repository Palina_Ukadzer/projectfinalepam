package com.epam.ukader.contraller.filter;

/**
 * Created by Palina Ukadzer on 10.06.2015.
 * Checks if encoding is UTF-8 and sets it to UTF-8 if needed
 */

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;

@WebFilter(urlPatterns = {"/"}, initParams = {@WebInitParam(name = "encoding",
        value = "UTF-8", description = "Encoding Param")})
public class EncodingFilter implements Filter {
    private String code;

    /**
     * gets parameter name encoding
     */
    public void init(FilterConfig fConfig) throws ServletException {
        code = fConfig.getInitParameter("encoding");
    }

    /**
     * Checks if encoding is UTF-8 and sets it to UTF-8 if needed
     */
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        String codeRequest = request.getCharacterEncoding();
        if (code != null && !code.equalsIgnoreCase(codeRequest)) {
            request.setCharacterEncoding(code);
            response.setCharacterEncoding(code);
        }
        chain.doFilter(request, response);
    }

    /**
     * Destroys private parameters
     */
    public void destroy() {
        code = null;
    }
}