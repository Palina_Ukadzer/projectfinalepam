<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 10.06.2015
  Time: 18:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="resources.prop"/>

<html lang="${language}">
<head>
    <title>See order</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="col-sm-10" align="center">
            <div class="col-sm-2">
                <form>
                    <input type="hidden" name="command" value="SeeOrder">
                    <select class="form-control" id="language" name="language" onchange="submit()">
                        <option style="color: #0f0f0f" value="ru" ${language == 'ru' ? 'selected' : ''}>RUS</option>
                        <option style="color: #0f0f0f" value="en" ${language == 'en' ? 'selected' : ''}>ENG</option>
                    </select>
                </form>
            </div>
        </div>
        <div class="col-sm-2">
            <form action="Controller">
                <input type="hidden" name="command" value="LogOut">
                <fmt:message key="logOut" var="logOutButton"/>
                <input class="btn-danger form-control" type="submit" value="${logOutButton}" id="block3">
            </form>
        </div>
    </div>
</nav>
<div class="col-sm-12">
    <div class="col-sm-4"></div>
    <div class="col-sm-4">

<div align="center">
        <h2><fmt:message key="orderForm"/></h2>
    <div class="panel panel-default">
        <h5><fmt:message key="howManyPeople"/>? :</h5>
    <c:if test="${order.peopleNumber==1}">
        <label class="radio-inline"><input type="radio" name="peopleNumber" disabled required="required"
                                           checked value="1">1</label>
        <label class="radio-inline"><input type="radio" name="peopleNumber" disabled value="2">2</label>
        <label class="radio-inline"><input type="radio" name="peopleNumber" value="3" disabled>3</label><br>
    </c:if>
    <c:if test="${order.peopleNumber==2}">
        <label class="radio-inline"><input type="radio" name="peopleNumber" disabled required="required"
                                           value="1">1</label>
        <label class="radio-inline"><input type="radio" name="peopleNumber" checked disabled
                                           value="2">2</label>
        <label class="radio-inline"><input type="radio" name="peopleNumber" value="3" disabled>3</label><br>
    </c:if>
    <c:if test="${order.peopleNumber==3}">
        <label class="radio-inline"><input type="radio" name="peopleNumber" disabled required="required"
                                           value="1">1</label>
        <label class="radio-inline"><input type="radio" name="peopleNumber" value="2">2</label>
        <label class="radio-inline"><input type="radio" name="peopleNumber" checked
                                           value="3">3</label><br>
    </c:if>
</div>
        <div class="panel panel-default">
            <h5><fmt:message key="classId"/>? :</h5>
            <ctg:apartmentClassPanel isBlocked="false" chosenClassID="${order.classId}"/>
        </div>
        <fmt:message key="timeIn"/>? :<br> <input class="form-control" value="${order.timeIn}" disabled><br>
        <fmt:message key="timeOut"/>? :<br> <input class="form-control" value="${order.timeOut}" disabled><br>
</div>
        </div>
    </div>
<div class="col-sm-12">
    <div class="col-sm-5"></div>
    <div class="col-sm-2">
        <fmt:message key="back" var="backButton"/>
        <button type="button" class="btn-default form-control" name="${backButton}" onclick="history.back()">${backButton}</button>
    </div>
</div>
</body>
</html>