<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 10.06.2015
  Time: 10:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="resources.prop"/>

<html lang="${language}">
<head>
    <title>Checks</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="col-sm-10" align="center">
            <div class="col-sm-2">
                <form>
                    <input type="hidden" name="command" value="SeeChecks">
                    <select class="form-control" id="language" name="language" onchange="submit()">
                        <option style="color: #0f0f0f" value="ru" ${language == 'ru' ? 'selected' : ''}>RUS</option>
                        <option style="color: #0f0f0f" value="en" ${language == 'en' ? 'selected' : ''}>ENG</option>
                    </select>
                </form>
            </div>
        </div>
        <div class="col-sm-2">
            <form action="Controler">
                <input type="hidden" name="command" value="LogOut">
                <fmt:message key="logOut" var="logOutButton"/>
                <input class="btn-danger form-control" type="submit" value="${logOutButton}" id="block3">
            </form>
        </div>
    </div>
</nav>
<div class="col-sm-2"></div>
<div class="col-sm-8">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div align="center">
                <h1><fmt:message key="myChecks"/></h1>
            </div>
        </div>

        <div class="panel-body">
            <c:choose>
                <c:when test="${not empty checks}">
                    <div class="span8 tasks-table table-responsive col-sm-12">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-8">
                               <table class="table table-hover" width="100">
                                <tr>
                                    <th></th>
                                    <th><fmt:message key="price"/></th>
                                </tr>
                                <c:forEach items="${checks}" var="check">
                                    <tr>
                                        <form action="Controller" method="post">
                                            <input type="hidden" name="command" value="SeeOrder">
                                            <input type="hidden" name="orderId" value="${check.orderId}">
                                            <fmt:message key="seeOrder" var="seeOrderButton"/>
                                            <td><input type="submit" class="btn-info" value="${seeOrderButton}"
                                                       id="block1"></td>
                                        </form>

                                        <td>${check.price}</td>
                                    </tr>

                                </c:forEach>

                            </table>
                        </div>
                    </div>

                </c:when>
                <c:otherwise>
                    <div class="col-sm-12">
                        <div align="center">
                            <h3><fmt:message key="noChecks"/></h3>
                        </div>
                    </div>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</div>
<div class="col-sm-12">
    <div class="col-sm-5"></div>
    <div class="col-sm-2">
        <fmt:message key="back" var="backButton"/>
        <button type="button" class="btn-default form-control" name="${backButton}"
                onclick="history.back()">${backButton}</button>
    </div>
</div>

</body>
</html>
