<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 02.06.2015
  Time: 15:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="resources.prop"/>

<html lang="${language}">
<head>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">

        <div class="col-sm-2" align="center">
            <form>
                <input type="hidden" name="command" value="LogIn">
                <select class="form-control" id="language" name="language" onchange="submit()">
                    <option style="color: #0f0f0f" value="ru" ${language == 'ru' ? 'selected' : ''}>RUS</option>
                    <option style="color: #0f0f0f" value="en" ${language == 'en' ? 'selected' : ''}>ENG</option>
                </select>

            </form>
        </div>
        <div class="col-sm-8" align="center">

        </div>
        <div class="col-sm-2">
            <form action="Controller">
                <input type="hidden" name="command" value="LogOut">
                <fmt:message key="logOut" var="logOutButton"/>
                <input class="btn-danger form-control" type="submit" value="${logOutButton}" id="block3">
            </form>
        </div>
    </div>
</nav>
<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div align="center">
                <h1><fmt:message key="myOrders"/></h1>
            </div>
        </div>
        <div class="panel-body">
            <c:choose>
                <c:when test="${not empty myOrders}">
                    <div class="span8 tasks-table table-responsive">
                        <table class="table table-hover" width="100">
                            <tr>
                                <th></th>
                                <th></th>
                                <th><fmt:message key="peopleNumber"/></th>
                                <th><fmt:message key="classId"/></th>
                                <th><fmt:message key="timeIn"/></th>
                                <th><fmt:message key="timeOut"/></th>
                            </tr>
                            <c:forEach items="${myOrders}" var="order">
                                <tr>
                                    <form action="Controller" method="post">
                                        <input type="hidden" name="command" value="DeleteOrder">
                                        <input type="hidden" name="orderId" value="${order.id}">
                                        <fmt:message key="delete" var="deleteButton"/>
                                        <td><input class="btn-info" type="submit" value="${deleteButton}" id="block1">
                                        </td>
                                    </form>

                                    <form action="Controller" method="post">
                                        <input type="hidden" name="command" value="ChangeOrder">
                                        <fmt:message key="change" var="changeButton"/>
                                        <input type="hidden" name="orderId" value="${order.id}">
                                        <td><input class="btn-info" type="submit" value="${changeButton}" id="block2">
                                        </td>
                                    </form>

                                    <td>${order.peopleNumber}</td>
                                    <ctg:apartmentClassField classId="${order.classId}"/>
                                    <td>${order.timeIn}</td>
                                    <td>${order.timeOut}</td>
                                </tr>

                            </c:forEach>
                        </table>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="col-sm-12">
                        <div align="center">
                            <h3><fmt:message key="noOrders"/></h3>
                        </div>
                    </div>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</div>
<div class="col-sm-12">
    <div class="col-sm-5"></div>
    <div class="col-sm-2">
        <form action="Controller">
            <input type="hidden" name="command" value="NewOrder">
            <fmt:message key="newOrder" var="newOrderButton"/>
            <td><input class="btn-success form-control" type="submit" value="${newOrderButton}" id="block4"></td>
        </form>
    </div>
</div>
<div class="col-sm-12">
    <div class="col-sm-5"></div>
    <div class="col-sm-2">
        <form action="Controller" method="post">
            <input type="hidden" name="command" value="SeeChecks">
            <fmt:message key="myChecks" var="checksButton"/>
            <input class="btn-primary form-control" type="submit" value="${checksButton}">
        </form>
    </div>
</div>
</body>
</html>
