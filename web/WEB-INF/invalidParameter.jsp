<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 30.06.2015
  Time: 18:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="resources.prop"/>

<html lang="${language}">
<head>
    <title>Error</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="col-sm-10" align="center">
            <div class="col-sm-2">
            </div>
        </div>
        <div class="col-sm-2">
            <form action="Controller">
                <input type="hidden" name="command" value="LogOut">
                <fmt:message key="logOut" var="logOutButton"/>
                <input class="btn-danger form-control" type="submit" value="${logOutButton}" id="block3">
            </form>
        </div>
    </div>
</nav>

<div align="center"><h1><fmt:message key="invalidParameter"/></h1></div>
<div class="col-sm-12">
    <div class="col-sm-5"></div>
    <div class="col-sm-2">
        <form action="Controller" method="post">
            <input type="hidden" name="command" value="LogIn">
            <fmt:message key="returnToFirstPage" var="toFirstButton"/>
            <input class="btn-primary form-control" type="submit" value="${toFirstButton}">
        </form>
    </div>
</div>

</body>
</html>
