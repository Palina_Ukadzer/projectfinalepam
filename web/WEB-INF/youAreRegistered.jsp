<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 04.06.2015
  Time: 14:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="resources.prop"/>

<html lang="${language}">
<head>
    <title>Registered</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="col-sm-10" align="center">
            <div class="col-sm-2">
                <form>
                    <input type="hidden" name="command" value="ChangeOrder">
                    <select class="form-control" id="language" name="language" onchange="submit()">
                        <option style="color: #0f0f0f" value="ru" ${language == 'ru' ? 'selected' : ''}>RUS</option>
                        <option style="color: #0f0f0f" value="en" ${language == 'en' ? 'selected' : ''}>ENG</option>
                    </select>
                </form>
            </div>
        </div>
    </div>
</nav>

<div align="center"><h1><fmt:message key="registered"/></h1></div>
<div align="center">
    <button type="button" name="back" onclick="history.back()">back</button>
</div>
<div align="center"><a href="hi"><fmt:message key="returnToLogIn"/></a></div>
</body>
</html>
