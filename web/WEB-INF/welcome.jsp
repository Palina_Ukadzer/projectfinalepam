<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 31.05.2015
  Time: 12:34
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="resources.prop"/>
<!DOCTYPE html>
<html lang="${language}">
<head>
    <title>Welcome</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">

        <div class="col-sm-2" align="center">
            <form>
                <select class="form-control" id="language" name="language" onchange="submit()">
                    <option style="color: #0f0f0f" value="ru" ${language == 'ru' ? 'selected' : ''}>RUS</option>
                    <option style="color: #0f0f0f" value="en" ${language == 'en' ? 'selected' : ''}>ENG</option>
                </select>

            </form>
        </div>

    </div>
</nav>
<div align="center">
    <form class="form-horizontal col-sm-12" action="Controller" method="post">
        <fieldset>
            <div class="col-sm-4"></div>
            <div class="panel panel-default form-group col-sm-4">
                <input type="hidden" name="command" value="LogIn">
                <div class="panel-heading"><h1><fmt:message key="welcome"/></h1></div>
                <div class="panel-body"><fmt:message key="email"/><br><input class="form-control"size="20" maxlength="40" name="Email"><br>
                <fmt:message key="password"/><br> <input class="form-control" type="password" size="20" name="Password" maxlength="20"
                                                         id="password"><br>
                <div style="margin-outside: 20px"><fmt:message key="logIn" var="logInButton"/>
               <input class="btn-default form-control" type="submit" value="${logInButton}"></div></div>
            </div>
        </fieldset>
    </form>
</div>




<div align="center">
    <form class="form-horizontal col-sm-12" action="Controller">
        <div class="col-sm-5"></div>


            <div class="form-group col-sm-2">
                <fmt:message key="register" var="registerButton"/>
                <div align="center">
                    <input type="hidden" name="command" value="Register">
                <input class=" btn-info form-control
                " type="submit" value="${registerButton}">
                    </div>
            </div>

    </form>
</div>
</body>
</html>
