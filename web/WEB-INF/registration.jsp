<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 04.06.2015
  Time: 14:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="resources.prop"/>

<html lang="${language}">
<head>
    <title><fmt:message key="register"/></title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
</head>
<body>
<div class="col-sm-12">
    <div class="col-sm-2">
        <form>
            <select class="form-control" id="language" name="language" onchange="submit()">
                <option style="color: #0f0f0f" value="ru" ${language == 'ru' ? 'selected' : ''}>RUS</option>
                <option style="color: #0f0f0f" value="en" ${language == 'en' ? 'selected' : ''}>ENG</option>
            </select>

        </form>
    </div>
</div>

<div class="col-sm-12">
    <div align="center">
        <h2><fmt:message key="registerForm"/></h2>
    </div>
</div>

<div class="col-sm-12">
    <div class="col-sm-4"></div>
    <div class="col-sm-4">
        <div align="center">
            <form autocomplete="off" action="Controller" method="post">
                <input type="hidden" name="command" value="AddUser">

                <fmt:message key="name"/> : <br><input class="form-control" size="20" maxlength="40" name="name"
                                                       required placeholder="Name"><br>
                <fmt:message key="surname"/> :<br> <input class="form-control" size="20" maxlength="40" name="surname"
                                                          placeholder="Surname" required=""><br>
                <fmt:message key="email"/> :<br> <input type="email" class="form-control" size="20" maxlength="40"
                                                        name="email" placeholder="email" required=""><br>
                <fmt:message key="phone"/> :<br> <input class="form-control" size="20" maxlength="40" name="phone"
                                                        placeholder="1231212" required=""><br>
                <fmt:message key="password"/> :<br> <input class="form-control" size="20" maxlength="40" name="password"
                                                           placeholder="1234567890" type="password" required><br>
                <input type="submit" value="Register" id="block7">
            </form>
        </div>
    </div>
</div>
<div class="col-sm-12">
    <div class="col-sm-5"></div>
    <div class="col-sm-2">
        <fmt:message key="back" var="backButton"/>
        <button type="button" class="btn-default form-control" name="${backButton}" onclick="history.back()">${backButton}</button>
    </div>
</div>

</body>
</html>
