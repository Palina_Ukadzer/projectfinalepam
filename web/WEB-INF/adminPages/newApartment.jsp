<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 20.06.2015
  Time: 14:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="resources.prop"/>
<%@ taglib prefix="ctg" uri="customtags" %>

<html lang="${language}">
<head>
    <title>New Apartment</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">

        <div class="col-sm-2" align="center">
            <form>
                <input type="hidden" name="command" value="CreateApartment">
                <select class="form-control" id="language" name="language" onchange="submit()">
                    <option style="color: #0f0f0f" value="ru" ${language == 'ru' ? 'selected' : ''}>RUS</option>
                    <option style="color: #0f0f0f" value="en" ${language == 'en' ? 'selected' : ''}>ENG</option>
                </select>

            </form>
        </div>
        <div class="col-sm-8" align="center">

        </div>
        <div class="col-sm-2">
            <form action="Controller">
                <input type="hidden" name="command" value="LogOut">
                <fmt:message key="logOut" var="logOutButton"/>
                <input class="btn-danger form-control" type="submit" value="${logOutButton}" id="block3">
            </form>
        </div>
    </div>
</nav>


<div class="col-sm-12">
    <div align="center">
        <h2><fmt:message key="fillInTheForm"/></h2>
    </div>
</div>

<div class="col-sm-12">
    <div class="col-sm-4"></div>
    <div class="col-sm-4">
        <div align="center">
            <form autocomplete="off" action="Controller" method="post">
                <input type="hidden" name="command" value="CreateApartment">
                <div class="panel panel-default">
                    <h5><fmt:message key="howManyPeople"/>? :</h5>
                    <label class="radio-inline"><input type="radio" name="peopleNumber" required="required" value="1">1</label>
                    <label class="radio-inline"><input type="radio" name="peopleNumber" value="2">2</label>
                    <label class="radio-inline"><input type="radio" name="peopleNumber" value="3">3</label><br></div>
                <div class="panel panel-default">
                    <h5><fmt:message key="classId"/>? :</h5>
                    <ctg:apartmentClassPanel/></div>
                <fmt:message key="price"/> :<input class="form-control" size="20" maxlength="40" name="price" type="number" required>
                <fmt:message key="addApartment" var="addApartmentButton"/>
                <input type="submit" class="btn-info form-control" value="${addApartmentButton}" id="block7">
            </form>
        </div>
    </div>
</div>
<div class="col-sm-12">
    <div class="col-sm-5"></div>
    <div class="col-sm-2">
        <fmt:message key="back" var="backButton"/>
        <button type="button" class="btn-default form-control" name="${backButton}" onclick="history.back()">${backButton}</button>
    </div>
</div>

</body>
</html>
