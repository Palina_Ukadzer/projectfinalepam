<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 06.06.2015
  Time: 6:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="resources.prop"/>

<html lang="${language}">
<head>
    <title>Choosing apartment</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="col-sm-10" align="center">
            <div class="col-sm-2">
                <form>
                    <input type="hidden" name="command" value="ProcessOrder">
                    <select class="form-control" id="language" name="language" onchange="submit()">
                        <option style="color: #0f0f0f" value="ru" ${language == 'ru' ? 'selected' : ''}>RUS</option>
                        <option style="color: #0f0f0f" value="en" ${language == 'en' ? 'selected' : ''}>ENG</option>
                    </select>
                </form>
            </div>
        </div>
        <div class="col-sm-2">
            <form action="Controller">
                <input type="hidden" name="command" value="LogOut">
                <fmt:message key="logOut" var="logOutButton"/>
                <input class="btn-danger form-control" type="submit" value="${logOutButton}" id="block3">
            </form>
        </div>
    </div>
</nav>
<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div align="center">
                <h1><fmt:message key="suitableApartments"/></h1>
            </div>
        </div>
        <div class="panel-body">


            <c:if test="${not empty apartments}">
                <div class="span8 tasks-table table-responsive col-sm-12">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-8">

                        <table class="table table-hover" width="100">
                            <tr>
                                <th></th>
                                <th><fmt:message key="price"/></th>
                            </tr>
                            <c:forEach items="${apartments}" var="apartment">
                                <tr>
                                    <form action="Controller">
                                        <td><fmt:message key="choose" var="chooseButton"/>
                                            <input type="hidden" name="orderId" value="${orderId}">
                                            <input type="hidden" name="apartmentId" value="${apartment.id}">
                                            <input type="hidden" name="command" value="ChooseThisApartment">
                                            <input type="submit" class="btn-info" value="${chooseButton}" id="block1">
                                        </td>
                                    </form>
                                    <td>${apartment.price}</td>
                                </tr>
                            </c:forEach>
                        </table>
                    </div>
                </div>
            </c:if>
        </div>
    </div>
</div>
<div class="col-sm-12">
    <div class="col-sm-5"></div>
    <div class="col-sm-2">
        <fmt:message key="back" var="backButton"/>
        <button type="button" class="btn-default form-control" name="${backButton}"
                onclick="history.back()">${backButton}</button>
    </div>
</div>


</body>
</html>
